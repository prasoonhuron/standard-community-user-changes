/*
 * @author      Sandeep Kadoor
 * @date        March 21, 2014
 * @description Component for displaying 
 */
public with sharing class ADMApplicationComponentController extends ADMBaseComponentController {

    public ADMApplicationComponentController() {isStartEnabled = false;}
    
    
   /**
    * added this as part of ADM-4236, to explicitly pass the public user id since pUserAccountInfo.
    * publicUser will be null for internal users
    */
     public ID publicUserId {get;set;}
    /**
     *  The variable passed to the component, it is used to differentiate between programs
     *  E.g. Fellowship, Admission
     */
    public String pProgramType {get; set;}
    
    /*
     *  This is passed from the component on various actions to identify the program code
     *  E.g. When applicant clicks on start application against MBA the program code will be mba
     */
    public String programCode {get; set;}   
    
    /*
     *  This is passed to the component from the page, this determines whether multiple application is allowed in a year for a Program Type
     *  Eg. for Application Program code this will be false and for Fellowship this will be true
     */
    public Boolean allowMultipleAppsInYear {get; set;}
    
    /*
     *  This is passed from the component on various actions to identify the application
     *  E.g. unsubmit application
     */
    public Id applicationId {get; set;}
    
    /*
     *  This is passed from the component on various actions to identify the round
     *  E.g. start application
     */
    public Id applicationStartRound {get; set;}

    /*
     * The selected round for which application has to be switched to
     */ 
    public String switchToRound {get; set;}

    /*
     * Programs for which switch round instructions needs to be displayed
     */ 
    public String programsForSwitchInstrsDisplay {
        get{
           return ADMConstants.PROGRAMCODE.msx.name()+CoreConstants.DELIMITER_COMMA+ADMConstants.PROGRAMCODE.mbamsx.name();
        }
        set;
    }


    /*
     * Help text for switch rounds
     */
    public String switchRoundsHelpText {
        get {
            return  TM_TEXT_OBJ.get(String.format(ADMConstants.TM_HOME_PAGE_SWITCH_ROUND_HELPTEXT_MESSAGE, new List<String>{pProgramType.toLowerCase()}));
        }
        set;
    }
    
    public String pageSectionDescription {
        get {
            return  TM_TEXT_OBJ.get(String.format(ADMConstants.TM_HOME_PAGE_PROGRAM_TYPE_SECTION_DESCRIPTION, new List<String>{pProgramType.toLowerCase()}));
        }
    }
    
    /**
     *  indicates whether component needs to be displayed or not
     */
    public Boolean display { 
        get {
            return isApplicationAvailable();
        } set;
    }
    
    /**
     * @description Public user username
     *              NOTE: THIS SHOULD BE BE PASSED IN THE URL PARAMETER FOR IMPERSONATION PURPOSES
     */
    public String publicUserName { get; set; }

    /*
     *  This is passed from the component.
     */
    public Boolean gdprConsentStatus {get; set;}

    /*
     *  This is passed from the component.
     */
    public String applicationYear {get; set;}

    
    /**
     * Visa form message
     * 
     * @return  String
     */
    public String visaFormMessageText {
        get {
            return this.TM_TEXT_OBJ.get(ADMConstants.TM_HOME_PAGE_VISA_FORM_MESSAGE);
        }
    }
    
    /**
     * Visa form message
     * 
     * @return  String
     */
    public String componentTitle {
        get {
            return TM_TEXT_OBJ.get(String.format(ADMConstants.TM_HOME_PAGE_APPLICATION_SECTION_TITLE, new List<String>{pProgramType.toLowerCase()}));
        }
    }

    /**
     * GDPR privacy statement text
     * 
     * @return  String
     */
    public String gdprPrivacyStatementText {
        get {
            return TM_TEXT_OBJ.get(ADMConstants.TM_HOME_PAGE_GDPR_PRIVACY_STATEMENT_TEXT);
        }
    }
    
    public String URL_APP_VIEW_TEXT { get { return 'View Application'; }}
    public String URL_APP_EDIT_TEXT { get { return 'Edit Application'; }}
    public String URL_APP_DECISION_TEXT { get { return 'View Decision'; }}
    public String URL_APP_NEXT_STEPS_TEXT { get { return 'View Next Steps'; }}
    public String URL_APP_UNSUBMIT_TEXT { get { return 'Unsubmit Application'; }}
    public String URL_APP_INTERVIEW_TEXT { get {return 'Update Interview Info' ; }}
    public String START_FINANCIAL_CERTIFICATION_FOR_VISA_LINK { get { return 'Start'; }}
    public String EDIT_FINANCIAL_CERTIFICATION_FOR_VISA_LINK { get { return 'Edit'; }}
    public String VISA_REQUEST_FORM { get { return 'Visa Request Form'; }}
    public String NEW_INFORMATION { get { return 'Provide New Information'; }}
    public String VIEW_UPLOADED_DOCUMENTS { get { return 'View Uploaded Documents'; }}
    
    
    
    /**
     *  Service class initializaton
     */
    public ADMHomePageService homePageService {
        get{
            if (homePageService == null){
                homePageService = new ADMHomePageService();
            }
            return homePageService;
        }
        set;
    }
    
    /**
     *  List of applications to be displayed on the home page.
     */
    public List<ADMHomePageService.ADMHomePageApplicationsVO> applications {
        get{
            User currentloggedinuser = [Select Id,ContactId from User where Id=:UserInfo.getUserId()];
            //Commented out this line on 08/28/2019 as we are using standard User object instead of Custom Public User
            //applications = processApplications(homePageService.getApplications(pUserAccountInfo.publicUser.Id,  pProgramType, currentYear));
            applications = processApplications(homePageService.getApplications(currentloggedinuser.ContactId,  pProgramType, currentYear));
            if (applications!=null) applications =  new ADMHomePageService().getUploadedDocuments(applications);
            return applications;
        }
        set;
    }
    
    
   

    /**
     *  List of Applicants recommenders and the status of each recommender to be displayed on the home page.
     */
    public List<ADMHomePageService.ADMHomePageApplicationRecommendersVO> recommnderStatusByApplication {
        get{
            User currentloggedinuser = [Select Id,ContactId from User where Id=:UserInfo.getUserId()];
            //Commented out this line on 08/28/2019 as we are using standard User object instead of Custom Public User
            //recommnderStatusByApplication =
            //homePageService.getRecommendationStatusByApplicant(pUserAccountInfo.publicUser.Id,null,null);
            recommnderStatusByApplication = homePageService.getRecommendationStatusByApplicant(currentloggedinuser.ContactId,null,null);
            return recommnderStatusByApplication;
        }   
        set;
    }
    
    /**
     *  Message to display to the user if no rounds are open
     */
    public String noRoundsOpenMsg {
        get {           
            String noRoundMsg = TM_TEXT_OBJ.get(String.format(ADMConstants.TM_HOME_PAGE_PROGRAM_TYPE_NO_ROUNDS_OPEN_MESSAGE, new List<String>{pProgramType.toLowerCase()}));
            if(String.isEmpty(noRoundMsg)) noRoundMsg = TM_TEXT_OBJ.get(ADMConstants.TM_HOME_PAGE_NO_ROUNDS_OPEN_MESSAGE);
            return noRoundMsg;
        }
    }
    
    /**
     *  @desc   If any rounds open for Program Type (for Program Type refer pProgramType variable description)
     */
    public Boolean isRoundsOpen {
        get {
            return ((rounds != null && rounds.size() > 0) ? true : false);
        }
    }
    
    /**
     *  All the rounds which are currently open for specific Progran Type
     *  E.g. if there are 2 rounds open for Application (one each for MBA and MSx) the list will contain both
     */
    public List<adm_round__c> rounds {
        get {
            if (rounds == null) {
                Datetime endtime = Datetime.now();
                List<adm_round__c> roundsTemp = ADMRoundService.getOpenRoundsByProgramType(pProgramType, true, endtime);
                Map<String, adm_round__c> prgTypeToRoundmap = getUniqueProgramCodeMap(roundsTemp);
                rounds = prgTypeToRoundmap.values();
            }
            return rounds;
        } set;
    }
    
    /**
     *  Set of round Id extracted from rounds list. We will use this set whenever we need collection of rounds for the comparison purposes
     */
    public Set<Id> roundIds {
        get {
            if (roundIds == null) {
                Set<Id> roundIdsTemp = new Set<Id>();
                for(adm_round__c admRound : rounds) {
                    roundIdsTemp.add(admRound.Id);
                }
                roundIds = roundIdsTemp;
            }
            return roundIds;
        } set;
    }
    
    /**
     *  Get the current Year. Assumption here is all rounds will have one year.
     */
    public String currentYear {
        get {
            if( (rounds != null) && (rounds.size() >0) ) {
                adm_round__c round = rounds.get(0);
                if(round != null) {
                     currentYear = round.year__c;
                }
            }
            
            return currentYear;
        } set;
    }
    

    /** This method returns the rounds for the current year for all the program types(mba,msx,mbamsx)
     * For the select option round id is returned as roundID@programCode as we need the programCode
     * information in the client side code
    * @description Returns Application Rounds List<SelectOption> property
    * 
    */
    
    public List<SelectOption> roundsForAllProgramTypes{
        get{ 
             List<adm_round__c> rounds = ADMRoundService.getRoundsForAllProgramsInYear(
                  new List<String>{ADMConstants.PROGRAMCODE.mba.name(),ADMConstants.PROGRAMCODE.msx.name(),ADMConstants.PROGRAMCODE.mbamsx.name()},currentYear);
             List<SelectOption> admRoundOptions = new List<SelectOption>();
             admRoundOptions.add(new SelectOption(CoreConstants.BLANK_STRING, CoreConstants.DEFAULT_ONE));
             for(adm_round__c round : rounds) {
                admRoundOptions.add(new SelectOption(round.Id+CoreConstants.DELIMITER_AT+round.program_code__c.toLowerCase(), round.Name));                        
             }         
             return admRoundOptions;    
        }
    }
    
    
     /**
     * Get visa form section details
     * 
     * @return  List of ADM Home Page Visa form view object
     */
    public List<ADMHomePageService.ADMHomePageVisaFormVO> visaForm {
        get {
            
            if(visaForm == null || visaForm.isEmpty()) {
                visaForm = getVisaFormForUser();
                visaForm = visaForm == null ? new List<ADMHomePageService.ADMHomePageVisaFormVO>() : visaForm;
            }
            return visaForm;    
        }
        set;
    }
    
    /**
     *  private list of all existing application for the user against Program Type
     */
    @TestVisible
    private List<ADMHomePageService.ADMHomePageApplicationsVO> existingApplications {
        get{
            if (existingApplications == null && publicUserId != null) {
                existingApplications = homePageService.getApplications(publicUserId,  pProgramType, currentYear);
            }else if (existingApplications == null && publicUserId == null){
            //Commented out this line on 08/28/2019 as we are using standard User object instead of Custom Public User
            //existingApplications = homePageService.getApplications(pUserAccountInfo.publicUser.Id,  pProgramType, currentYear);
            User currentloggedinuser = [Select Id,ContactId from User where Id=:UserInfo.getUserId()];
            existingApplications = homePageService.getApplications(currentloggedinuser.ContactId,  pProgramType, currentYear);
            }
            return existingApplications;
        }  
        set;
    }
    
    /**
     *  @desc   Text Manager Map
     */
    private Map<String, String> TM_TEXT_OBJ {
        get {
            if (TM_TEXT_OBJ == null) {
                getTMSettingAndContent(pProgramType);               
            }
            return TM_TEXT_OBJ;
        } 
        set;
    }


    /**
     *  Message to display if there are applications not started
    */
    public String startApplicationMessage {
        get {   
            String startAppMsg = TM_TEXT_OBJ.get(String.format(ADMConstants.TM_HOME_PAGE_PROGRAM_TYPE_START_APP_MESSAGE, new List<String>{pProgramType.toLowerCase()}));
            if(String.isEmpty(startAppMsg)) startAppMsg = TM_TEXT_OBJ.get(ADMConstants.TM_HOME_PAGE_START_APP_MESSAGE);
            return startAppMsg;
        }
    }
    
    /**
     * Unsubmit Application Message
     * 
     * @return  String
     */
    public String unSubmitAppMessage
    {
        get 
        {
            return this.TM_TEXT_OBJ.get(ADMConstants.TM_HOME_UNSUBMIT_APP_MESSAGE);
        }
        set;
    }


    /**
     *  Boolean to tell if there are any start applications still.
    */
    public boolean isStartEnabled{ get; set;}
    
    /**
     *  This method is trigger when we click on start fellowship application .
    */    
    public Pagereference startFellowshipApplication(){
        allowMultipleAppsInYear = true;
        System.debug('<<<<<startApplication()>>>>>'+startApplication());
        return startApplication();
    }
    
    /**
     *  @desc   Starts the application:
     *                  Entry Criteria: Application start should be allowed - Refer isStartApplicationAllowed method description
     *          Also this method navigates the user to an additional instructions page for msx .
     *
     *   Common Logic has been moved to homepage Service class startApplicationLogic method.
     */
    public Pagereference startApplication() {
        ADMRoundService roundService = new ADMRoundService();
        adm_round__c round = roundService.getRoundsById(new List<Id> {applicationStartRound});
        System.debug('<<<<<round>>>>>'+round);
        if (isStartApplicationAllowed()) {
            System.debug('<<<<<ApplicationAllowed>>>>>');
            if(!ADMConstants.PROGRAMCODE.msx.name().equalsignoreCase(round.program_code__c) && !ADMConstants.PROGRAMCODE.mbamsx.name().equalsignoreCase(round.program_code__c)){
                System.debug('<<<<<startApplicationLogicIF>>>>>');
                return homePageService.startApplicationLogic(round, this.pUserAccountInfo, gdprConsentStatus);
            }else{
                Map<String, String> urlParameters = new Map<String, String>();
                urlParameters.put(ADMConstants.URL_PARAM_NAME.program.name(), round.program_code__c.toLowerCase());
                urlParameters.put(ADMConstants.URL_PARAM_NAME.proundid.name(), CoreUtils.encryptString(applicationStartRound));
                urlParameters.put(ADMConstants.URL_PARAM_NAME.gdpr.name(), CoreUtils.encryptString(String.valueOf(gdprConsentStatus)));
                return CoreUtils.redirectPage(ADMConstants.URL_APPLICANT_START_APPLICATION_MSX_MBAMSX, urlParameters);
            }
        } else return null;
    }
    
    /** 
     * Remote Action to unsubmit application
     *
     * @returns PageReference
     */
    public PageReference unSubmitApplication()
    {
        homePageService.unsubmitApplication(applicationId, programCode,applicationYear);
        existingApplications = null;
        return null;
    }
    
     /**
     * Get Visa form the current user
     *
     * @param Id            public User Id
     * @param programCode   programCode
     * @param currentRound  current round
     * 
     * @return  List of ADM Home Page Visa form view object
     */
    public List<ADMHomePageService.ADMHomePageVisaFormVO> getVisaFormForUser() {
        List<Id> applicationIdList = new List<Id>();
        List<ADMHomePageService.ADMHomePageVisaFormVO> lstVO = new List<ADMHomePageService.ADMHomePageVisaFormVO>();
        if (existingApplications != null) {
            for(ADMHomePageService.ADMHomePageApplicationsVO appVo :existingApplications) {
                if(!appVo.isDecisionLetterViewed) continue;
                applicationIdList.add(appVo.Id);
            }               
        }
        if (applicationIdList != null) {
            lstVO = homePageService.getVisaForm(applicationIdList);
            assembleVisaFormUrls(lstVO);
        }
        return lstVO;
    }
    
    
    public PageReference viewDecision () {
        homePageService.updateOnViewDecision(applicationId);
        Map<String, String> urlParameters = new Map<String, String>();       
        urlParameters.put(ADMConstants.URL_PARAM_NAME.program.name(), programCode);
        urlParameters.put(ADMConstants.URL_PARAM_NAME.pappid.name(), CoreUtils.encryptString(applicationId));
        if (this.pUserAccountInfo.isImpersonate) {
            if (String.isNotEmpty(this.publicUserName)) urlParameters.put(ADMConstants.URL_PARAM_NAME.pusername.name(), 
                this.publicUserName);
        }               
        return CoreUtils.redirectPage(ADMConstants.URL_APPLICANT_VIEW_DECISION_PAGE, urlParameters);
    }
    /**
     *  @desc   This method determines if starting a application is allowed or not
     *                  <i>roundsToConsider</i> This is list of rounds against which if application is exista then start is not allowed.
     *                  <i>allowMultipleAppsInYear</i> Refer variable comments
     *                  Logic: 
     *                  1.  allowMultipleAppsInYear = true THEN the Start Application is allowed if 
     *                      round against which Start application is clicked should not have application created (E.g. Start Africa is allowed if not created already)
     *                  2.  allowMultipleAppsInYear = true THEN start application is allowed only if no rounds has application already
     *                      E.g. for Applications component if there are 2 Rounds, MBA and MSX, then none of the rounds should have active application. Once
     *                      a application created for one round applcant can not create application against other round
     */
    private Boolean isStartApplicationAllowed() {
        List<Id> roundsToConsider;
        if(allowMultipleAppsInYear) {
            roundsToConsider = new List<Id> {applicationStartRound};
        } else{
            roundsToConsider = new List<Id> (roundIds);
        }
        ADMApplicationService admAppService = new ADMApplicationService();
        //List<adm_application__c> appList = admAppService.getApplicationsByUserAndRounds(pUserAccountInfo.publicUser.Id, roundsToConsider, null, false);
        User currentloggedinuser = [Select Id,ContactId from User where Id=:UserInfo.getUserId()];
        List<adm_application__c> appList = admAppService.getApplicationsByUserAndRounds(currentloggedinuser.ContactId, roundsToConsider, null, false);
        return (appList != null && appList.size() > 0 ) ? false : true;
    }
    
    /**
     *  @desc       Processes the list of applications and adds required URL.
     *                      Determines the list of rounds for which Start Link needs to be displayed
     *
     *  @param  lstAppVO    List of applications user already created
     *
     *  @return List        We are expicitly returning the list since we are adding new elements to the list
     */
    private List<ADMHomePageService.ADMHomePageApplicationsVO> processApplications(List<ADMHomePageService.ADMHomePageApplicationsVO> lstAppVO){
        List<Id> roundsWithApplications = new List<Id>();//Holds List of Round Ids which has application
        Boolean isApplicantHasCurrentYearApp = false;//Useful if Multiple application in a year is not allowed
        adm_applicant__c applicantInfo;
        if(lstAppVO != null && lstAppVO.size() > 0) {
            for (ADMHomePageService.ADMHomePageApplicationsVO appVo : lstAppVO) {
                if(roundIds.contains(appVo.roundId)) {
                    roundsWithApplications.add(appVo.roundId);
                }
                if(appVo.applicationYear.equalsIgnoreCase(currentYear)) {
                    isApplicantHasCurrentYearApp = true;
                }
            }           
        } else {
            lstAppVO = new List<ADMHomePageService.ADMHomePageApplicationsVO>();
        }
        assembleApplicationUrl(lstAppVO);
        if(isApplicantHasCurrentYearApp && !allowMultipleAppsInYear){
            return lstAppVO;// Applicant has current year application
        } else {
            Set<Id> roundsWithoutApps = roundIds.clone(); //Clone as we need original list intact
            roundsWithoutApps.removeall(roundsWithApplications);//After remove whatever rounds left is rounds without application   
            ADMApplicantService service = new ADMApplicantService();
            User currentloggedinuser = [Select Id,ContactId from User where Id=:UserInfo.getUserId()];
            List<adm_applicant__c> applicantInfoList = service.getApplicantByUserIds(new List<Id> {currentloggedinuser.ContactId});    
            if (!applicantInfoList.isEmpty())    
                applicantInfo = applicantInfoList[0];
            for(ADMHomePageService.ADMHomePageApplicationsVO vo : createVos(roundsWithoutApps, rounds, applicantInfo)) {
                if (lstAppVO.isEmpty()) {
                    lstAppVO.add(vo);// Create VO entry for remaining round, this will enable users to have Start Application
                } else {
                    lstAppVO.add(0, vo);
                }
            }
        } 
        return lstAppVO;
    }
    
    /**
     *  @desc   Based on the criteria this method provides URL to the application and it which will be used to display in the page
     *
     *  @param  lstVos  List of applications user already created
     *
     *  @return void
     */
    private void assembleApplicationUrl(List<ADMHomePageService.ADMHomePageApplicationsVO> lstVos) {        
        if(lstVos != null && lstVos.size() > 0) {   
            Map<String, String> varmap;
            for (ADMHomePageService.ADMHomePageApplicationsVO appVo : lstVos) {
                varmap = createURLParams(appVo);
                if (appVo.isViewEnabled) {
                    appVo.viewAppUrl = Site.getPathPrefix() + CoreUtils.redirectPage(ADMConstants.URL_APPLICANT_VIEW_APPLICATION, varmap).getUrl();
                }   
                if (appVo.isInterviewEnabled) appVo.interviewUrl = Site.getPathPrefix() + CoreUtils.redirectPage(ADMConstants.URL_APPLICANT_INTERVIEW_PAGE, varmap).getUrl();
                if (appVo.isEditEnabled) appVo.editAppURL = Site.getPathPrefix() + CoreUtils.redirectPage(ADMConstants.URL_APPLICANT_EDIT_APPLICATION, varmap).getUrl();
                if (appVo.isUnSubmitEnabled) appVo.unSubmitUrl = '';
                if (appVo.isDecisionEnabled) appVo.decisionUrl = Site.getPathPrefix() + CoreUtils.redirectPage(ADMConstants.URL_APPLICANT_VIEW_DECISION_PAGE, varmap).getUrl();
                if (appVo.isNextStepsEnabled) appVo.nextStepsUrl = Site.getPathPrefix() + CoreUtils.redirectPage(ADMConstants.URL_APPLICANT_NEXT_STEPS_PAGE, varmap).getUrl();
            }
        }       
    }   
    
    /**
     *  @desc   Create VO for the rounds which do not have applications. This will be displayed on page with Start Application Link
     *
     *  @param  roundsWithoutApps   Collection of Rounds without application created
     *  @param  currentRounds       Collection of currently open rounds
     *
     *  @return Collection of VOs
     */
    private List<ADMHomePageService.ADMHomePageApplicationsVO> createVos (Set<Id> roundsWithoutApps, List<adm_round__c> currentRounds, adm_applicant__c applicantInfo) {
        List<ADMHomePageService.ADMHomePageApplicationsVO> voList = null;
        if(roundsWithoutApps != null && currentRounds != null) {            
            voList = new List<ADMHomePageService.ADMHomePageApplicationsVO>();
            ADMHomePageService.ADMHomePageApplicationsVO vo;
            for(adm_round__c cr : currentRounds) {
                if (roundsWithoutApps.contains(cr.Id)) {
                    vo = new ADMHomePageService.ADMHomePageApplicationsVO();
                    vo.isStartEnabled = true;
                    vo.roundId = cr.Id;
                    vo.programCode = cr.program_code__c;
                    vo.program = cr.program__c;
                    vo.applicationYear = cr.year__c;
                    vo.applicantStatus = ADMConstants.APPLICATION_STATUS.Not_Started.name().replace(CoreConstants.DELIMITER_UNDERSCORE, CoreConstants.DELIMITER_SPACE);
                    if (applicantInfo != null)
                        vo.gdprConsent = applicantInfo.gdpr_consent__c;
                    voList.add(vo); // Fix  for ADM-2023
                }
            }
        }

        // Code added as part of ticket 2163. This is used to specify whether to show start app message or not.
        if(voList.size() > 0 && !voList.isEmpty() && !String.isBlank(pProgramType) && ADMConstants.PROGRAMTYPE.admission.name().equalsIgnoreCase(pProgramType)){
            isStartEnabled = true;
        }

        return voList;
    }
    
    /**
     *  @desc   This methods decides whether to render the component or not
     *          As of now Only for Fellowship display criteria exists  
     */
    private Boolean isApplicationAvailable(){
        //Continue only if Fellowship else component is displayed
        if (pProgramType.equalsIgnoreCase(ADMConstants.PROGRAMTYPE.fellowship.name())){
            Pagereference pageRef = ApexPages.currentPage();
            MAP<String, String> urlParams = pageRef.getParameters();
            String prgCode = null;
            if (urlParams != null && 
                urlParams.containsKey(ADMConstants.URL_PARAM_NAME.program.name())) {
                prgCode = urlParams.get(ADMConstants.URL_PARAM_NAME.program.name());
            }
            if (prgCode != null &&  
                    prgCode.equalsIgnoreCase(ADMConstants.PROGRAMTYPE.fellowship.name())){
                        //Coming from Fellowship Link
                return true;
            }
            else {
                if ((existingApplications != null) && (existingApplications.size() > 0))
                    return true;//Already has fellowship application
                else 
                    return false; //Neither coming from Fellowship link nor has fellowship application
            }           
        }
        else
            return true;// not Fellowship? Always display
    }
    
    /**
     * @desc    URL params for Application Links
     */
    private Map<String, String> createURLParams(ADMHomePageService.ADMHomePageApplicationsVO vo) {
        Map<String, String> urlParameters = new Map<String, String>();       
        urlParameters.put(ADMConstants.URL_PARAM_NAME.program.name(), vo.programCode);
        if (String.isNotEmpty(vo.Id)) urlParameters.put(ADMConstants.URL_PARAM_NAME.pappid.name(), CoreUtils.encryptString(vo.Id));
        if (this.pUserAccountInfo.isImpersonate) {
            if (String.isNotEmpty(this.publicUserName)) urlParameters.put(ADMConstants.URL_PARAM_NAME.pusername.name(), 
                this.publicUserName);
        }
        return urlParameters;
    }
    
    /**
     * @desc    URL params for Visa Links
     */
    private Map<String, String> createURLParams(ADMHomePageService.ADMHomePageVisaFormVO vo) {
        Map<String, String> urlParameters = new Map<String, String>();       
        urlParameters.put(ADMConstants.URL_PARAM_NAME.program.name(), vo.programCode);
        if (String.isNotEmpty(vo.Id)) urlParameters.put(ADMConstants.URL_PARAM_NAME.pappid.name(), CoreUtils.encryptString(vo.Id));
        if (this.pUserAccountInfo.isImpersonate) {
            if (String.isNotEmpty(this.publicUserName)) urlParameters.put(ADMConstants.URL_PARAM_NAME.pusername.name(), 
                this.publicUserName);
        }
        return urlParameters;
    }
    
    /**
     * Retrieve both application settings and text manager content
     *
     * @param   progrtype Program type - admission, mba
     *
     * @return  None
     */
    private void getTMSettingAndContent(String progType) {
        TM_TEXT_OBJ = new Map<String, String>();
        if(progType != null){
            TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(),
                                ADMConstants.TM_HOME_PAGE));
        }
    }
    
    /**
     * @description Adds Urls to Applicaiton View Object records
     *
     * @param lstAppVO  List of application view objects
     *
     * @return void
     **/
    private void assembleVisaFormUrls(List<ADMHomePageService.ADMHomePageVisaFormVO> lstAppVO) {
        for (ADMHomePageService.ADMHomePageVisaFormVO appVo : lstAppVO) {
            if (appVo.isVisaFormCreate) appVo.createVisaFormURL = Site.getPathPrefix() + CoreUtils.redirectPage(ADMConstants.URL_VISA_FORM_PAGE, createURLParams(appVo)).getUrl();
            if (appVo.isEditVisaForm) appVo.editVisaFormURL = Site.getPathPrefix() + CoreUtils.redirectPage(ADMConstants.URL_VISA_FORM_PAGE, createURLParams(appVo)).getUrl();
        }
    } 
    
    private Map<String, adm_round__c> getUniqueProgramCodeMap(List<adm_round__c> roundList) {
        Map<String, adm_round__c> prgTypeToRoundmap = new Map<String, adm_round__c>();
        if(roundList != null && !roundList.isEmpty()) {
            for(adm_round__c r : roundList){
                if (!prgTypeToRoundmap.containsKey(r.program_code__c)) {
                    prgTypeToRoundmap.put(r.program_code__c, r);
                }
            }
        }
        return prgTypeToRoundmap;
    }
}