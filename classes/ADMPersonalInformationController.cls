/**
 * Admissions Personal and Family Informtion Controller class
 *
 * @author     Anil Sure
 * @date       March 21, 2014
 * @decription Controller class for admissions family and personal information
 *
 */
public class ADMPersonalInformationController extends ADMBasePageController { 
     
    /*****************************************************************************************************************************
     *
     * Constants & Private Variables
     *
     *****************************************************************************************************************************/
    private Map<String, String> TM_MESSAGES_MAP = new Map<String, String>();
    private Map<String, String> TM_ERROR_TEXT = new Map<String, String>();
    private Map<String, String> TM_SETTING_OBJ = new Map<String, String>();
    private Set<String> setCountriesNeedStates = new Set<String>();
   
     
    /*****************************************************************************************************************************
     *
     * Properties
     *
     *****************************************************************************************************************************/

    /**
     * @description ADM Family object
     */
    public ADMFamilyService.FamilyVO admFamily { get; set; }    
     
     /**
     * @description ADM Children object
     */
    public ADMFamilyService.ChildrenVO admChildren { get; set; }
    
     /**
     * @description ADM FamilyMember object
     */
    public ADMFamilyService.GSBConnectionVO admGSBConnection { get; set; }
    
     /**
     * @description ADM Spouse object
     */
    public ADMFamilyService.SpouseVO admSpouse { get; set; }
    
    /**
     * @description ADM ParentsAndGuardiansVO collection
     */
    public List<ADMFamilyService.FamilyVO> admCurrentParents { get; set; }
    
    /**
     * @description ADM ChildrenVO collection
     */
    public List<ADMFamilyService.ChildrenVO> admCurrentChildren { get; set; }
    
    /**
     * @description ADM GSBConnectionVO collection
     */
    public List<ADMFamilyService.GSBConnectionVO> admCurrentGSBConnections { get; set; }
    
    /**
     * @description Application Personal information
     *  Personal related information stored in the application object
     */
    public ADMApplicationService.PersonalInformationVO personalInformation { get; set; }
    
    /**
     * @description Applicant Personal information
     *  Personal related information stored in the applicant object
     */
    public ADMApplicantService.PersonalInformationVO applicantPersonalInformation { get; set; }
    
    /**
     * @description Exception messages object
     */
    public ADMPageExceptionMessages exceptionMessagesObj { get; set; }
    
    /**
     * @description subSectionTobeSaved parameter
     */
    public string subSectionToBeSaved { get; set; }
    
    /**
     * @description subSectionTobeDeleted parameter
     */
    
    public string subSectionToBeDeleted { get; set; }     
    
    /**
     * @description Countries List<SelectOption> property
     */
    public List<SelectOption> countries { get { return CoreUtils.getCountries(false); } }
    
    /**
     * @description Countries List<SelectOption> property
     */
    public List<SelectOption> birthCountries { get { return CoreUtils.getCountries(); } }
    
    /**
      * @description CitizenShipStatus List<SelectOption> property
      */
    public List<SelectOption> citizenshipstatus { get { return CoreUtils.getCitizenshipStatus(false); } }
     /**
      * @description ADM Rounds List<SelectOption> property
      */
    public List<SelectOption> admSORounds{
    get{ 
         List<adm_round__c> roundOptions = ADMRoundService.getRoundsForYear(this.programCode, String.valueof(this.admApplication.round_id__r.year__c));
         List<SelectOption> admRoundOptions = new List<SelectOption>();
         for(adm_round__c round : roundOptions) {
         	admRoundOptions.add(new SelectOption(round.Id, round.Name));         	         	
         }         
         return admRoundOptions;    
        }
    }
    
     /**
      * @description ADM Rounds List<SelectOption> property
      */
    public List<SelectOption> admOtherApplicantRounds{
        get{ 
             List<adm_round__c> roundOptions = ADMRoundService.getRoundsForYear(this.programCode, String.valueof(this.admApplication.round_id__r.year__c));
             List<SelectOption> admRoundOptions = new List<SelectOption>();
             for(adm_round__c round : roundOptions) {
             	admRoundOptions.add(new SelectOption(round.Id, round.Name));         	         	
             }         
             return admRoundOptions;    
        }
    }
    /**
     * @description States List<SelectOption> property
     *              Only get for the selected current countries
     */
    public Map<String, List<SelectOption>> states { get {
        	return CoreUtils.getStates(this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_COUNTRIES_NEED_STATES, 
                new List<String>{this.programCode})).split(CoreConstants.DELIMITER_COMMA), false, true, false);
        } 
    }
    /**
     * @description BirthStates List<SelectOption> property
     *              Only get for the selected birth countries
     */
    public Map<String, List<SelectOption>> birthStates { get {
        	return CoreUtils.getStates(this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_COUNTRIES_NEED_STATES, 
                new List<String>{this.programCode})).split(CoreConstants.DELIMITER_COMMA), true, true);
        } 
    }    
    /**
     * @description New Property
     */    
    public Boolean isNew { get; set; }   
    /**
     * @description variable to bind the selected country in the VF pages
     */
    public String selectedCountry { get; set; }    
    /**
     * @description List of countries that need states dropdown
     */
    public String listOfCountriesNeedStates {
        get { 
            return this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_COUNTRIES_NEED_STATES, 
                    new List<String>{programCode}));
        }
        set;
    }
    /**
      * TM - Text Manager content property
      */
    public Map<String, String> textManagerContent {
         get { return TM_MESSAGES_MAP; }
    }    
    /**
	 * @description	Maximum parents property
     */
	public Integer MAX_PRENTS {
		get {
			Integer MAX_PRENTS = ADMConstants.DEFAULT_MAX_PARENTS;
			try {
				MAX_PRENTS = Integer.ValueOf(TM_MESSAGES_MAP.get(String.format(ADMConstants.TM_APP_FAMILY_MAX_PARENTS, 
							new List<String>{programCode})));
			}
			catch (Exception ex) {}
			return MAX_PRENTS;
		}
	}	
	/**
	 * @description	Maximum Children property
	 */
	public Integer MAX_CHILDREN {
		get {
			Integer MAX_CHILDREN = ADMConstants.DEFAULT_MAX_CHILDREN;
			try {
				MAX_CHILDREN = Integer.ValueOf(TM_MESSAGES_MAP.get(String.format(ADMConstants.TM_APP_FAMILY_MAX_CHILDREN, 
							new List<String>{programCode})));
			}
			catch (Exception ex) {}
			return MAX_CHILDREN;
		}
	}
	
	/**
	 * @description	Maximum FamilyMember property
	 */
	public Integer MAX_GSB_CONNECTIONS {
		get {
			Integer MAX_GSB_CONNECTIONS = ADMConstants.DEFAULT_MAX_GSB_CONNECTIONS;
			try {
				MAX_GSB_CONNECTIONS = Integer.ValueOf(TM_MESSAGES_MAP.get(String.format(ADMConstants.TM_APP_FAMILY_MAX_GSB_CONNECTIONS, 
							new List<String>{programCode})));
			}
			catch (Exception ex) {}
			return MAX_GSB_CONNECTIONS;
		}
	}
	
	/**
     *  Max Number of Results
     */
    public String maxNumResults 
    {
        get
        {
            return TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_MAXIMUM_AUTOCOMPLETE_SEARCH_RESULTS, 
                new List<String>{programCode}));
        }
        set;
    }
    /**
     *   Registration Survey Object
     */    
    public adm_applicant__c  survey;    
     /**
     * URL parameters
     */ 
    public Map<String, String> urlParameters {get; set;} 
    /**
     * Primary CitizenShip Country Info
     */ 
    public string primaryCitizenShipCountry;
    /**
     * Current Country Info
     */ 
    public string currentCountry;
    
     /**
     * Birth Country Info
     */ 
    public Id birthCountry;
    /**
     *@description This is used to store the attachment id of the file being deleted
	**/
    public Id attachmentId {get; set;}
    
	/**
     *@description This is used to store the file name of the attachment being deleted
    **/ 
    public String attachmentName {get; set;} 
    
    /**
 	 * Parent Attend college field pick list value - Received a degree
 	 */ 
	public String receivedDegree {
		get {
		    return ADMConstants.PARENT_ATTEND_COLLEGE.Received_a_degree.name().replace(CoreConstants.DELIMITER_UNDERSCORE,CoreConstants.DELIMITER_SPACE);
		}
		set;
    }
    
    /**
 	 * Parent Attend college field pick list value - Attended but did not graduate
 	 */ 
	public String attendedButDidNotGraduate {
		get {
		    return ADMConstants.PARENT_ATTEND_COLLEGE.Attended_but_did_not_graduate.name().replace(CoreConstants.DELIMITER_UNDERSCORE,CoreConstants.DELIMITER_SPACE);
		}
		set;
    }
    
    /**
 	 * ADMQuestionAnswerComponentController instance
 	 */
 	public ADMQuestionAnswerComponentController questionAnswerComponentController { get; set; }
 	
 	/**
 	 * Override component controller class setter method in ADMBasePageController class with ADMQuestionAnswerComponentController reference
 	 */
 	public override void setComponentController(ADMBaseComponentController compBaseController) {
    	questionAnswerComponentController = (ADMQuestionAnswerComponentController) compBaseController;
  	}
	
	/**
 	 * Override component controller class getter method in ADMBasePageController class with ADMQuestionAnswerComponentController reference
 	 */
  	public override ADMBaseComponentController getComponentController() {
    	return questionAnswerComponentController;
  	}
     
	  
    /*****************************************************************************************************************************
     *
     * Constructors
     *
     *****************************************************************************************************************************/
    public ADMPersonalInformationController() {
    	super.initialize();
    	getTMSettingAndContent(this.programCode);
    	initialize();
    }
    
    /*****************************************************************************************************************************
     *
     * Action Methods
     *
     *****************************************************************************************************************************/
    /**
     * Delete Parents and Guardians information
     *
     * @return  Page reference object
     */
    public override PageReference deleteObj() {
    	
    	// Call parent controller delete object routine
		super.deleteObj();
        //before delete save the unsaved applicant/application/spouse data
        ADMFamilyService parentService = new ADMFamilyService(this.programCode, this.vfPageRequiredFields, this.vfPageVisibleFields);
        if(subSectionToBeDeleted.equalsIgnoreCase(ADMConstants.PERSONAL_FAMILY_SUB_SECTION_NAME.parentSection.name())){
            parentService.deleteParents(admCurrentParents, this.personalInformation, this.applicantPersonalInformation, this.admSpouse, this.navigationSectionId, this.TM_ERROR_TEXT, setCountriesNeedStates);
        }
        //before delete save the unsaved applicant/application/spouse data
        if(subSectionToBeDeleted.equalsIgnoreCase(ADMConstants.PERSONAL_FAMILY_SUB_SECTION_NAME.childrenSection.name())){
            parentService.deleteChildren(admCurrentChildren, this.personalInformation, this.applicantPersonalInformation, this.admSpouse, this.navigationSectionId, this.TM_ERROR_TEXT, setCountriesNeedStates);
        }
        //before delete save the unsaved applicant/application/spouse data
        if(subSectionToBeDeleted.equalsIgnoreCase(ADMConstants.PERSONAL_FAMILY_SUB_SECTION_NAME.gsbSection.name())){
            parentService.deleteGSBConnections(admCurrentGSBConnections,this.personalInformation, this.applicantPersonalInformation, this.admSpouse, this.navigationSectionId, this.TM_ERROR_TEXT, setCountriesNeedStates);
        } 
		//creating warning for deletion of add parent/gaurdian
		if(subSectionToBeDeleted.equalsIgnoreCase(ADMConstants.PERSONAL_FAMILY_SUB_SECTION_NAME.parentSection.name()))
		createWarning();
        // Refresh page
		return CoreUtils.redirectPage(this.navigationUrl, super.createUrlParameters());
    }
    /**
     *creating warning for deletion of add parent/gaurdian
     *
     * @return none
     */
    private void createWarning() {
        ADMExceptionService excService = new ADMExceptionService();
        List<adm_application_exception__c> warnings = new List<adm_application_exception__c>();
             
        if(admCurrentParents.size() <= 1)
        {
             warnings.add(excService.create(personalInformation.application.Id, navigationSectionId, personalInformation.application.Id, 'Name', 
              ADMConstants.EXCEPTION_TYPE.Warning.name(), TM_ERROR_TEXT.get(String.format(
              ADMConstants.TM_APP_ERROR_APPLICANT_FAMILY_INFO_PARENT_WARNING, new List<String>{this.programCode}))));
        }
         if (!warnings.isEmpty()) insert warnings;
    } 
    
	/**
     * Initializes the Parents and Guardians/Children/FamilyMember/Spouse section
     *
     * @return  None
     */        
     public override void initialize() {
     	
     	//List of application personal Information VOs
		List<ADMApplicationService.PersonalInformationVO> personalInfos;
		//List of applicant personal Information VOs
		List<ADMApplicantService.PersonalInformationVO> applicantPersonalInfos;        
        // Instance for Applicant Personal Information Service
        ADMApplicantService applicantService = new ADMApplicantService(this.programCode);
        // Instance for Application Personal Information Service
        ADMApplicationService applicationService = new ADMApplicationService(this.programCode);
        // Instance for Family Information Service
        ADMFamilyService familyService = new ADMFamilyService(this.programCode);
        // Instance for ADM Exception Service
        ADMExceptionService expService = new ADMExceptionService();
        // Instance for ADM Registration Survey Service
        ADMRegistrationSurveyService regSurvey = new ADMRegistrationSurveyService();
        //Get the reg survey details for current public user
        //survey = regSurvey.getExistingSurvey(this.admApplicant.public_user_id__r.user_name__c);
         survey = regSurvey.getExistingSurvey(this.admApplicant.Contact__r.email);
        //getTMSettingAndContent(this.programCode);
        setCountriesNeedStates.addAll(this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_COUNTRIES_NEED_STATES, 
        			new List<String>{this.programCode})).toLowerCase().split(CoreConstants.DELIMITER_COMMA));
        //Get the United Staes Country Info
       	l_country__c usaCountry = coreUtils.getCountryInfoByCountryName(ADMConstants.ADM_DEFAULT_COUNTRY);
        // Prepopulate the ADM Registration Survey data on Initial Load
    	// Check for the page is first save or not and registration Survey is having the data 
        if (!admApplication.is_personal_info_first_save__c && (survey.gender__c != null || survey.primary_citizenship__c != null || survey.country_id__c != null || survey.city__c != null)) {
        	
        	//Make US as default contry if no country is selected
        	if(usaCountry != null && this.admApplicant.birth_country_id__c == null){ 
        		birthCountry = usaCountry.Id;
        	}
            else{
                birthCountry = this.admApplicant.birth_country_id__c;
            } 
                    	
        	this.applicantPersonalInformation = new ADMApplicantService.PersonalInformationVO(survey.gender__c, birthCountry);        	
        	
        	//Set selectedCurrentCountryId as US if it's defaulted to United States to fetch the related States
        	if(birthCountry == usaCountry.Id) {
        		this.applicantPersonalInformation.selectedCountryId = birthCountry;
        		this.applicantPersonalInformation.selectedCountryName = usaCountry.country__c;
        	}        	
        	//Assign the base page controller applicant to applicant personalinformation VO
        	this.applicantPersonalInformation.applicant = this.admApplicant;
        	//set the gender value to display default selected value in the picklist        	
        	applicantPersonalInformation.setgender(survey.gender__c);
        	//set the birth country value to display default selected value in the picklist 
        	applicantPersonalInformation.setBirthCountry(birthCountry);       	
        	//check for citizenshipstatus if it is blank then make the country default to United States
        	if(usaCountry != null && survey.primary_citizenship__c != null) primaryCitizenShipCountry = survey.primary_citizenship__r.country__c;        		
        		else primaryCitizenShipCountry = usaCountry.country__c;
        	//check for birth Country if it is blank then make the country default to United States
        	if(usaCountry != null && survey.country_id__c != null) 	currentCountry = survey.country_id__r.country__c;
            	else currentCountry = usaCountry.country__c;         	       	
        	     	
        	this.personalInformation = new ADMApplicationService.PersonalInformationVO(primaryCitizenShipCountry, currentCountry, survey.city__c);
        	//Set selectedBirthCountryId as US if it's defaulted to United States to fetch the related States
        	if(currentCountry == usaCountry.country__c) {
        		this.personalInformation.selectedCurrentCountryId = currentCountry;
        	}
        	//Assign the base page controller application to application personalinformation VO 
        	this.personalInformation.application = this.admApplication;        	
        	//set the PrimaryCitizenship value to display default selected value in the picklist
        	personalInformation.setPrimaryCitizenship(primaryCitizenShipCountry);
        	//set the CurrentCountry value to display default selected value in the picklist
        	personalInformation.setCurrentCountry(currentCountry);
        	//set the CurrentCity value to display default selected value in the picklist
        	personalInformation.setCurrentCity(survey.city__c);
        	
        } else {        	
        	// High level Applicant personal information
			applicantPersonalInfos = applicantService.getPersonalInformationByApplicantIds(new List<Id>{this.admApplicant.Id}, this.admApplicant.gender__c, setCountriesNeedStates, this.admApplicant.birth_country_id__c);
			//set the gender value to display selected value in the picklist    
			applicantPersonalInfos[0].setgender(this.admApplicant.gender__c);
			//set the country value to display selected country in the picklist 
			applicantPersonalInfos[0].setBirthCountry(this.admApplicant.birth_country_id__c);
			//check for birth country if blank then make the country default to United States			
	        if(usaCountry != null && applicantPersonalInfos[0].applicant.birth_country_id__c == null) {
	        	birthCountry = usaCountry.id;
				applicantPersonalInfos[0].selectedCountryName = usaCountry.country__c;
	        }
	        else applicantPersonalInfos[0].selectedCountryName = this.admApplicant.birth_country_id__r.country__c;
			if (applicantPersonalInfos != null && !applicantPersonalInfos.isEmpty()) this.applicantPersonalInformation = applicantPersonalInfos[0];
    		else this.applicantPersonalInformation = new ADMApplicantService.PersonalInformationVO(survey.gender__c, birthCountry);
    		    		     	
	        // High level Application personal information
			personalInfos = applicationService.getPersonalInformationByApplicationIds(new List<Id>{this.admApplication.Id}, this.admApplication.primary_citizenship__c, this.admApplication.current_country__c, this.admApplication.current_city__c, setCountriesNeedStates);
			personalInfos[0].setPrimaryCitizenship(this.admApplication.primary_citizenship__c);
        	personalInfos[0].setCurrentCountry(this.admApplication.current_country__c);
        	personalInfos[0].setCurrentCity(this.admApplication.current_city__c);
        	//check for citizenshipstatus if it is blank then make the country default to United States
        	if(survey.primary_citizenship__c != null) primaryCitizenShipCountry = survey.primary_citizenship__r.country__c;        		
        		else primaryCitizenShipCountry = usaCountry.country__c;
        	//check for currennt Country if it is blank then make the country default to United States
        	if(survey.country_id__c != null) currentCountry = survey.country_id__r.country__c;        	
             	else currentCountry = usaCountry.country__c; 																	
			if (personalInfos != null && !personalInfos.isEmpty()) this.personalInformation = personalInfos[0];	        		
			else this.personalInformation = new ADMApplicationService.PersonalInformationVO(primaryCitizenShipCountry, currentCountry, survey.city__c);
    	}
    	
		
		// High level Family information
		List<Id> applicationIds = new List<Id>{this.personalInformation.application.Id};			
		this.admCurrentParents = familyService.getParentsByApplicationIds(applicationIds, MAX_PRENTS, this.expandId);			  
        this.admCurrentChildren = familyService.getChildrenByApplicationIds(applicationIds, MAX_CHILDREN, this.expandId);
        this.admCurrentGSBConnections = familyService.getGSBConnectionByApplicationIds(applicationIds, MAX_GSB_CONNECTIONS, this.expandId);
		this.admFamily = familyService.initializeParents(admCurrentParents, MAX_PRENTS);
		this.admChildren = familyService.initializeChildren(admCurrentChildren, MAX_CHILDREN);
		this.admGSBConnection = familyService.initializeGSBConnections(admCurrentGSBConnections, MAX_GSB_CONNECTIONS);
			
		// High level Spouse information
		List<ADMFamilyService.SpouseVO> spouseInfos = familyService.getSpouseByApplicationIds(applicationIds);		
        
		if (spouseInfos != null && !spouseInfos.isEmpty()) {
			this.admSpouse = spouseInfos[0];
		} else {
			this.admSpouse = new ADMFamilyService.SpouseVO();
			this.admSpouse.Spouse.application_id__c = personalInformation.application.Id;	        			
		}			
		
		// Load exception messages
		List<Id> subSectionIds = new List<Id>();
		// Add application personalInformation VO to subSection IDs
		subSectionIds.add(personalInformation.application.Id);
		// Add applicant personalInformation VO to subSection IDs
		subSectionIds.add(applicantpersonalInformation.applicant.Id);
		// Add spouse VO to subSection IDs
		subSectionIds.add(admSpouse.Spouse.Id);			
		// Add parent VO to subSection IDs
		for (ADMFamilyService.FamilyVO familyVO : this.admCurrentParents) {
			subSectionIds.add(familyVO.family.Id);
		}
		// Add children VO to subSection IDs
		for (ADMFamilyService.ChildrenVO childrenVO : this.admCurrentChildren) {
			 subSectionIds.add(childrenVO.children.Id);
        }
        // Add familymember VO to subSection IDs
        for (ADMFamilyService.GSBConnectionVO gsbConnectionVO : this.admCurrentGSBConnections) {
        	subSectionIds.add(gsbConnectionVO.gsbConnection.Id);
        }
		this.exceptionMessagesObj = new ADMPageExceptionMessages(expService.getExceptions(applicationIds, 
			new List<String>{navigationSectionId}, null, CoreConstants.DELIMITER_UNDERSCORE));
         System.Debug('<<<<<thisexceptionMessagesObj>>>>>'+this.exceptionMessagesObj);
		// Reset expand id
		this.expandId = null;
		// format birth date
		if (String.isNotEmpty(String.valueOf(this.applicantPersonalInformation.applicant.birthdate__c))) this.applicantPersonalInformation.birthDate = this.applicantPersonalInformation.applicant.birthdate__c.format();
		
     }
         
    /**
     * Save Parents and Guardians information
     *
     * @return  Page reference object
     */
    public override PageReference save() {
        System.Debug('<<<<<ENTEREDSAVE>>>>>>');
    	//Check if to create essential objects and url parameters
		PageReference pageref = super.save();
        System.Debug('<<<<<pageref>>>>>>'+pageref);
		if(pageRef == null)
		//Save Personal and Family Information		
			savePersonalFamilyInformation(); 
		else	
			return pageRef;
        System.Debug('<<<<<thisnavigationUrl>>>>>>'+this.navigationUrl);
        System.Debug('<<<<<supercreateUrlParameters>>>>>>'+super.createUrlParameters());
		// Refresh page
		return CoreUtils.redirectPage(this.navigationUrl, super.createUrlParameters());
    }

    /**
     * Submit application
     *
     * @return  Page reference object
     */
    public override PageReference submit() {
		return super.submit();
    }

    /*****************************************************************************************************************************
     *
     * Remote Action Methods
     *
     *****************************************************************************************************************************/
	/**
     * @description    Remote action to fetch cities
     *
     */
     @RemoteAction 
     public static List<adm_city__c> fetchCityOptions(String country, String city, String strNumRecs)
     {
        ADMCityService service = new ADMCityService();
        Integer numRecs = String.isEmpty(strNumRecs) ? ADMConstants.DEFAULT_MAX_AUTOCOMPLETE_SEARCH_RESULTS : Integer.valueOf(strNumRecs);
        return service.getCitiesBasedOnCountry(country, city, numRecs);
     }
	
    
    /*****************************************************************************************************************************
     *
     * Private Methods
     *
     *****************************************************************************************************************************/

    /**
     * Retrieve Text Manager contents
     *
     * @return  None
     */
    private void getTMSettingAndContent(String programCode) {
		// Get page instructions and common user setting identifiers              
		this.TM_MESSAGES_MAP = TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_FAMILY, new List<String>{programCode}));
		this.TM_MESSAGES_MAP.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_ALL, new List<String>{programCode})));
    	this.TM_MESSAGES_MAP.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
			String.format(ADMConstants.TM_APP_PANEL_HEADER_NO_NAME_PROVIDED, new List<String>{programCode})));		
     	// Get error messages
    	this.TM_ERROR_TEXT = TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
        	String.format(ADMConstants.TM_APP_ERROR_APPLICANT_FAMILY, new List<String>{programCode}));   	
    	// Get application settings
    	this.TM_SETTING_OBJ = TextManagerUtils.getAppSettingAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
        	String.format(ADMConstants.TM_APP, new List<String>{programCode}));
        // Load error messages for used by visualforce page
        this.TM_MESSAGES_MAP.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_ERROR_APPLICANT_FAMILY_INFO_MILITARY_DOC, new List<String>{programCode})));
		this.TM_ERROR_TEXT.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_ERROR_APPLICANT, new List<String>{programCode})));
   	}
   	
   /**
	 * Save PersonalFamilyInformation
	 *
	 * @return	None
	 */
	private void savePersonalFamilyInformation() {
		System.Debug('<<<<savePersonalFamilyInformation>>>>');
		ADMFamilyService familyService = new ADMFamilyService(this.programCode, this.vfPageRequiredFields, this.vfPageVisibleFields);
		System.Debug('<<<<thisprogramCode>>>>'+this.programCode);
        System.Debug('<<<<thisvfPageRequiredFields>>>>'+this.vfPageRequiredFields);
        System.Debug('<<<<thisvfPageVisibleFields>>>>'+this.vfPageVisibleFields);
        List<ADMFamilyService.FamilyVO> admParentsToBeSaved = null;
		//Collect list of parents to be saved 
        if (admFamily.isNew){
			admParentsToBeSaved =  new List<ADMFamilyService.FamilyVO>{this.admFamily};
            System.Debug('<<<<admParentsToBeSavedIF>>>>'+admParentsToBeSaved);
        } else	{     
			admParentsToBeSaved = admCurrentParents; 
            System.Debug('<<<<admParentsToBeSavedElse>>>>'+admParentsToBeSaved);
        }
		//Collect list of children to be saved			
		List<ADMFamilyService.ChildrenVO> admChildrenToBeSaved = null; 
		if (admChildren.isNew)
			admChildrenToBeSaved =  new List<ADMFamilyService.ChildrenVO>{this.admChildren};
		else	     
			admChildrenToBeSaved = admCurrentChildren;
		
		//Collect list of familymembers to be saved			
		List<ADMFamilyService.GSBConnectionVO> admGSBConnectionsToBeSaved = null; 
		if (admGSBConnection.isNew)
			admGSBConnectionsToBeSaved =  new List<ADMFamilyService.GSBConnectionVO>{this.admGSBConnection};
		else	     
			admGSBConnectionsToBeSaved = admCurrentGSBConnections;
		// Save Applicant, Application, Family, Spouse Information
		List<Id> secIds = familyService.savePersonalFamilyInformation(this.navigationSectionId, this.personalInformation, this.applicantpersonalInformation,
            admParentsToBeSaved,  admChildrenToBeSaved, admGSBConnectionsToBeSaved,this.admSpouse, this.TM_ERROR_TEXT, subSectionToBeSaved, setCountriesNeedStates);
        // Retrieve section id to be expanded
		if (!secIds.isEmpty()) this.expandId = String.valueOf(secIds[0]); 
		
		// Save question and answers
        if (FIELD_SECTION_SETTINGS_MAP.containsKey('ADMPersonalFamilyInformation.QUESTION_AND_ANSWER') && FIELD_SECTION_SETTINGS_MAP.get('ADMPersonalFamilyInformation.QUESTION_AND_ANSWER').isVisible) {
        	ADMQuestionAnswerService service = new ADMQuestionAnswerService();
        	service.saveQuestionAndAnswer(questionAnswerComponentController.questionAnswerVOList);
        }	        
	}

}