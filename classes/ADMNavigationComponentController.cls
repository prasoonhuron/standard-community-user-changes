/**
 * ADMNavigationComponentController
 * <p>
 * This class provides methods needed to populate left navigation bar component
 * </p>
 *
 * @author      Deepak Rama
 * @date        Mar 13, 2014
 */
public with sharing class ADMNavigationComponentController  extends ADMBaseComponentController
{
    public String activePageName { get; set; }
    
    public Boolean includeBadges { get; set; }
    
    /**
     * @description Program code property
     */
    public String programCode { get; set; }
    
    /*
     * Public Constructor
     *
     */
    public ADMNavigationComponentController()
    {
        this.programCode = System.currentPagereference().getParameters().get(ADMConstants.URL_PARAM_NAME.program.name());

    }
    
    /**
     * User role
     */
    public String pUserRole { get; set; }

    /**
     * List<ADMNavigationVO> Property
     *
     */
    public List<ADMNavigationVO> lstADMNavVO 
    { 
        get
        {
            ADMNavigationService svc =  new ADMNavigationService();
            // Derive user role and prepare navigation links if the navigation component does not hold the user role.
            if (String.isEmpty(pUserRole)) {
                // Get user role
                //ADMConstants.USER_ROLE userRole = getUserRole(pUserAccountInfo);
                ADMConstants.USER_ROLE userRole = getUserRole(UserInfo.getUserId());
                System.Debug('<<<<<activePageName>>>>>'+activePageName);
                //System.Debug('<<<<<objectDataId>>>>>'+objectData.Id);
                System.Debug('<<<<<thisprogramCode>>>>>'+this.programCode);
                System.Debug('<<<<<userRolename()>>>>>'+userRole.name());
                System.Debug('<<<<<includeBadges>>>>>'+includeBadges);
                //TODO: Add parameters
                lstADMNavVO = svc.getNavigationLinks(activePageName, objectData.Id , this.programCode,  userRole.name(), '', includeBadges, true);
            } else {
                lstADMNavVO = svc.getNavigationLinks(activePageName, objectData.Id , this.programCode,  pUserRole, '', includeBadges, false);
            }
            return lstADMNavVO;
        }
        
        set; 
    }
    
    /*****************************************************************************************************************************
     *
     * Private Methods
     *
     *****************************************************************************************************************************/
    
    /**
     * Currently this method checks only if the user role is either Applicant or Recommender. 
     * The method needs to be modified when the more user roles been added to the constant ADMConstants.USER_ROLE.
     * Below method depends on "ADMRecommendationsService" to derive the user's role, due to that method created here, and this method needs to be
     * moved to CoreUtils once the dependency removed.
     *
     * @param userAccountObj User Account object
     *
     * @return User role
     */
    private ADMConstants.USER_ROLE getUserRole(String Id){
        /*if(userAccountObj.isGSBAlumniLogin)
              return ADMConstants.USER_ROLE.interviewer;
        else{
            ADMRecommendationsService rService = new ADMRecommendationsService(this.programCode);
            return rService.isRecommender(userAccountObj.publicUser.Id) 
                        ? ADMConstants.USER_ROLE.recommender 
                        : ADMConstants.USER_ROLE.applicant;
        }*/
        User currentloggedinuser = [Select Id,ContactId from User where Id=:UserInfo.getUserId()];
        ADMRecommendationsService rService = new ADMRecommendationsService(this.programCode);
            return rService.isRecommender(currentloggedinuser.ContactId) 
                        ? ADMConstants.USER_ROLE.recommender 
                        : ADMConstants.USER_ROLE.applicant;
    }
    
}