/*
 * @author     KC Kwong
 * @date       February 21, 2014
 * @decription Selector class for application
 */
public with sharing class ADMApplicationSelector extends BaseSelector {
    /**
     * List of fields from ADM Application object
     */
    private final List<String> applicationFields = new List<String>{'Id', 'Name', 'applicant_id__c', 'aspire_to_do_after_mba__c', 
        'application_submitted_date__c', 'current_industry__c', 'desire_job_function__c', 'desire_industry__c', 'high_school_city__c', 'high_school_country__c', 
        'high_school_end__c',  'high_school_name__c', 'high_school_start__c', 'high_school_state__c', 'application_round_year__c', 
        'non_confidential__c', 'probation__c', 'resume_attachment_id__c', 'suspended__c', 'ug1_city__c', 'ug1_country__c', 
        'ug1_degree__c', 'ug1_school_id__c','ug1_degree_date__c', 'ug1_degree2__c', 'ug1_end__c', 'ug1_major__c', 'ug1_major_field__c', 
        'ug1_major_field2__c', 'ug1_major_2__c', 'ug1_name__c', 'ug1_school_code__c', 'ug1_second_major__c', 'ug1_state__c', 
        'ug1_gpa_cumulative__c','ug1_gpa_scale__c','ug1_gpa_first_year__c','ug1_gpa_second_year__c','ug_transcript_attachment_id__c',
        'ug1_gpa_third_year__c','ug1_gpa_fourth_year__c','ug1_gpa_fifth_year__c','class_rank__c','probation_suspended_attachment_id__c',        
        'fund_parents__c ','fund_scholarships__c','fund_loans__c','fund_employment__c', 'has_waiver__c', 'payment_decision__c', 'payment_mode__c', 
        'payment_status__c', 'payment_reason_code__c', 'payment_submitted_date__c', 'payment_transaction_code__c', 'payment_response_message__c', 
        'status__c', 'ug1_start__c', 'unemployed__c', 'work_gap_explanation__c', 'ops_interview_location__c',
        'activity_motivation__c','su_affiliate__c','so_also_applying__c', 'other_applicant__c', 'other_applicant_name__c', 'other_applicant_relationship__c', 
        'so_applicant_round_id__c', 'so_applicant_round_id__r.name', 'other_applicant_round_id__c', 'other_applicant_round_id__r.name', 'current_state__c', 'favorite_place__c', 'favorite_read__c', 'words_20__c', 'military__c', 
        'latino__c', 'tribal_number__c', 'primary_phone_country_code__c', 'primary_phone__c', 'primary_phone_ext__c', 'primary_phone_type__c', 'alt_phone_country_code__c',
        'alt_phone__c', 'alt_phone_ext__c', 'alt_phone_type__c', 'current_country__c', 'current_city_id__c','current_city__c', 'citizenship_status__c',
        'military_document_attachment_id__c', 'essay_attachment_id__c','secondary_citizenship__c ', 'primary_citizenship__c',
        'additional_information_attachment_id__c','accept_terms__c','applying_jt_degree__c','other_joint_degree__c', 'applicant_id__r.title__c', 
        'ethnicity_1__c','ethnicity_2__c', 'ethnicity_3__c','personal_statement_attachment_id__c','college_senior__c','cs_plans_after_college__c',
        'grad_student__c','p4d__c','msx__c','reapplicant__c','reapplicant_date__c','reapplicant_decision__c','round_id__c',
        'crime__c','crime_document_attachment_id__c','p4d_essay_attachment_id__c','cs_financial_aid_document_attachment_id__c','cs_deferral__c','is_college_senior_waiver__c', 
        'joint_degree__c', 'applicant_id__r.first_name__c','applicant_id__r.last_name__c', 'applicant_id__r.preferred_name__c', 'applicant_id__r.previous_first_name__c', 'applicant_id__r.previous_last_name__c', 
        'applicant_id__r.gender__c', 'applicant_id__r.name_pronunciation__c', 'applicant_id__r.birth_country_id__c', 'applicant_id__r.birth_state_id__c','applicant_id__r.birth_city__c', 
        'applicant_id__r.birthdate__c', 'tests_taken__c', 'gmat_retake__c', 'gmat_date__c', 'gmat_verbal_score__c', 'gmat_verbal_percent__c', 
        'gmat_quant_score__c', 'gmat_quant_percent__c', 'gmat_awa_score__c', 'gmat_awa_percent__c', 'gmat_ir_score__c', 'gmat_ir_percent__c', 'gmat_total_score__c', 
        'gmat_total_percent__c', 'gre_retake__c', 'gre_date__c', 'gre_verbal_score__c', 'gre_verbal_percent__c', 'gre_quant_score__c', 'gre_quant_percent__c', 
        'gre_awa_score__c', 'gre_awa_percent__c', 'english_test__c', 'toefl_retake__c', 'test_date__c', 'toefl_type__c', 'toefl_listening__c', 'toefl_speaking__c', 
        'toefl_reading__c', 'toefl_writing__c', 'toefl_total__c', 'ielts_retake__c', 'ielts_date__c', 'ielts_listening__c', 'ielts_reading__c', 'ielts_writing__c', 
        'ielts_speaking__c', 'ielts_total__c', 'pte_retake__c', 'pte_date__c', 'pte_listening__c', 'pte_reading__c', 'pte_speaking__c', 'pte_writing__c', 'pte_total__c', 
        'applicant_id__r.public_user_id__r.email__c', 'applicant_id__r.gsb_account_id__r.email__c','round_id__r.end_date_time__c', 'round_id__r.Name', 'round_id__r.year__c', 'is_personal_info_first_save__c', 
        'is_military_waiver__c', 'non_us_citizen_years__c', 'application_number__c', 'round_id__r.round_number__c', 'round_id__r.program_code__c', 'prof_exp_months_total__c',
        'is_acp__c', 'is_acp_z__c', 'is_rg__c', 'is_ecp__c', 'gmat_equivalent__c', 'applicant_id__r.public_user_id__r.first_name__c', 'applicant_id__r.public_user_id__r.last_name__c','applicant_status__c', 
        'interview_additional_information__c', 'interview_location__c', 'interviewer_id__c', 'interviewer_id__r.class_year__c', 'interviewer_id__r.first_name__c', 'interviewer_id__r.last_name__c', 
        'interviewer_id__r.preferred_email_address__c', 'interviewer_id__r.preferred_phone__c', 'interviewer_id__r.preferred_name__c', 'applicant_id__r.Name' ,'mailing_street_1__c', 
        'mailing_street_2__c', 'mailing_city__c', 'mailing_state_id__c', 'mailing_country_id__c', 'mailing_state_non_us__c', 'mailing_zip__c', 'is_interview__c', 
        'permanent_same_as_mailing__c', 'permanent_street_1__c', 'permanent_street_2__c', 'permanent_city__c', 'permanent_zip__c', 'permanent_state_id__c', 'permanent_country_id__c', 
        'city_timezone__c', 'interview_type__c', 'interview_timestamp__c', 'interviewer_matched_mail_timestamp__c', 'interviewer_unmatched_mail_timestamp__c', 'interview_schedule_mail_timestamp__c', 
        'invite_for_interview_mail_timestamp__c', 'first_reminder_mail_timestamp__c', 'post_interview_mail_timestamp__c', 'second_reminder_mail_timestamp__c', 'no_evaluation__c', 
        'round_id__r.academic_year_id__c', 'change_my_default_location__c', 'mailing_country_id__r.country__c', 'permanent_country_id__r.country__c',  'current_city_id__r.name__c', 'current_state_non_us__c', 'decision_posted_mail_timestamp__c',
        'director_decision__c', 'admit_conditions__c', 'plagiarism_document_id__c', 'uid__c', 'ug1_school_id__r.Name', 'transcript_requirement__c', 'decision_round_id__r.admit_weekend__c', 'defer_enrollment_year__c', 
        'yield_survey_link__c', 'round_id__r.waitpool_deadline__c', 'round_id__r.deposit_deadline__c ', 'decision_round_id__r.deposit_deadline__c', 'univid__c', 'director_decision_external_comments__c',
		'household_income__c','bank_accounts__c','home_equity__c','business_value__c','investments__c','retirement__c', 'interview_country_id__c', 'interview_state_id__c', 'interview_country_id__r.country__c',
        'gsb_account_id__c', 'is_rec_alum__c', 'rec_gsb__c', 'is_spin__c', 'class_year__c', 'allocation_category__c','current_job_function__c','current_job_title__c','current_job_salary__c','round_id__r.advertised_round_close__c','applicant_reminder_date__c',
        'applicant_id__r.middle_name__c', 'program_code__c', 'round_id__r.decision_posting_date__c', 'test_voucher_number__c', 'director_call_date__c','waitpool_round_id__c','waitpool_round_id__r.Id', 'have_you_taken_any_other_tests__c',
		'applied_fellowship_previous_year__c', 'describe_fellowship_opportunity__c', 'discover_fellowship_opportunity__c','was_english_primary_language__c','waitpool_round_id__r.waitpool_deadline__c',
		'graduation_fund_employment__c','graduation_fund_loans__c','graduation_fund_parents__c','graduation_fund_scholarships__c', 'number_of_dependents__c', 'board_exam_score_10th__c', 'board_exam_score_12th__c', 'common_admission_test_score__c',
		'comment_on_exam_if_necessary__c','joint_entrance_exam_score__c', 'round_id__r.program__c','work_experience_since_ug_months__c', 'work_experience_since_ug_years__c', 'months_work_experience__c',
		'round_id__r.program_type__c', 'non_confidential_explanation__c', 'degree_program__c', 'student_deferral_eligibility__c', 'direct_enrollment__c',
        'full_time_student__c','heartland__c','heartland_essay_attachment_id__c','military_branch__c','skype_id__c', 'sponsorship_status__c','terms_signature__c','tomodachi__c', 'joint_degree_status__c',
        'tomodachi_essay_attachment_id__c', 'gmat_reportcard_document_attachment_id__c','gre_reportcard_document_attachment_id__c','toefl_reportcard_document_attachment_id__c','ielts_reportcard_document_attachment_id__c',
        'pte_reportcard_document_attachment_id__c', 'no_supervisor_as_recommender__c', 'other_no_supervisor_as_recommender__c','payment_reference__c','fellowship_for__c', 'high_school_id__c', 'high_school_code__c','payment_amount__c', 'is_contact_info_first_save__c',
        'permanent_state_id__r.State__c', 'mailing_state_id__r.State__c', 'other_gpa_scale__c', 'msx_prof_exp_months_total__c', 'msx_not_enough_exp__c', 'is_msx_spin__c', 'msx_spin__c', 'msx_spin_contact__c', 'msx_spin_matches__c', 'spin_matches__c', 'original_conditions__c',
		'msx_director_decision__c', 'is_msx_interview__c', 'msx_application_evaluation_status__c', 'application_evaluation_status__c', 'mba_interview_status__c', 'msx_interview_status__c', 'msx_interview_type__c', 'msx_interviewer_id__c','msx_interview_timestamp__c',
        'msx_interviewer_matched_mail_timestamp__c', 'msx_interview_additional_information__c', 'msx_interview_location__c', 'msx_interviewer_id__r.class_year__c', 'msx_interviewer_id__r.first_name__c', 'msx_interviewer_id__r.last_name__c', 
        'msx_interviewer_id__r.preferred_email_address__c', 'msx_interviewer_id__r.preferred_phone__c', 'msx_interviewer_id__r.preferred_name__c', 'mba_ycbm_url__c', 'msx_ycbm_url__c', 'waitpool_round_id__r.program_code__c', 'decision_letter_id__c','decision_viewed_datetime__c',
        'waitpool_round_id__r.round_number__c', 'decision_letter_id__r.decision_letter__c', 'supplemental_interviewer_id__c', 'close_date__c', 'deposit_1_date__c', 'new_information__c','selected_fellowship_finalist__c','ug_college__c','applicant_decision_letter_message__c','sponsor_hr_contact__c','admit_stage__c',
        'msx_original_conditions__c','msx_admit_conditions__c','current_salary_currency__c','board_exam_10th_percent__c','board_exam_12th_percent__c','joint_entrance_exam_percent__c','common_admission_test_percent__c','interviewee_thanked__c',
        'current_job_other_income__c','current_other_income_currency__c','khs_number__c','africa_fellowship_attachment_id__c','africa_fellowship__c', 'decision_letter_joint_degree_message__c','current_student_major__c','additional_joint_dual_degree_interest__c',
		'application_round_number__c','clone_application_number__c'};// PK

    /**
     * List of selective fields (Application and Lookup fields) from ADM Application object for flags related functionality
     */
    private final List<String> applicationAndLookupFields = new List<String>{ 'Id','Name', 'program_code__c', 'selected_fellowship_finalist__c', 'citizenship_status__c', 'ethnicity_1__c', 'ethnicity_2__c', 'ethnicity_3__c',
        'military__c','college_senior__c','ug1_end__c','application_round_year__c','round_id__r.round_number__c', 'applicant_id__r.public_user_id__r.first_name__c', 'is_rec_alum__c', 'rec_gsb__c', 'is_spin__c', 'class_year__c', 'is_msx_spin__c', 'msx_spin__c', 'msx_spin_contact__c', 'msx_spin_matches__c', 'spin_matches__c', 'original_conditions__c','sponsorship_status__c', 'unemployed__c', 
        'prof_exp_months_total__c', 'msx_prof_exp_months_total__c', 'msx_not_enough_exp__c', 'is_acp__c', 'is_acp_z__c', 'is_rg__c', 'is_ecp__c', 'round_id__r.enrollment_start_date__c', 'director_decision__c', 'is_interview__c','latino__c','primary_citizenship__c', 'allocation_category__c', 'degree_program__c', 'full_time_student__c', 'ug1_name__c',
        ' (SELECT Id,Name, application_id__c, child_age__c, child_gender__c, child_name__c, '+
        ' gsb_connection_degree__c,gsb_connection_last_name__c, gsb_connection_name__c,gsb_connection_relationship__c, gsb_connection_year__c, '+ 
        ' other_so_relationship__c, parent_attend_college__c,parent_college__c, parent_degree__c, parent_gender__c, parent_grad_degree__c, RecordTypeId, RecordType.DeveloperName, '+
        ' parent_grad_school__c, parent_job__c,  parent_living__c, parent_name__c, parent_org__c, parent_relationship__c, so_name__c, so_relationship__c, first_name__c, last_name__c  '+
        ' FROM ADM_Families__r), '+
        ' (SELECT stanford_mba_class_year__c, employer__c, job_title__c, rec_alum__c,rec_gsb__c,first_name__c, last_name__c from ADM_Recommendations__r), ' +
        ' (SELECT Id, Name, start_date_internal__c, organization_Id__r.is_extern__c from ADM_Employers__r order by start_date_internal__c DESC) '};

    /**
     * Order by clause
     */
    private final List<String> orderBy = new List<String> {'CreatedDate DESC'};

    /*****************************************************************************************************************************
     *
     * Constructors
     *
     *****************************************************************************************************************************/
    public ADMApplicationSelector() {}

    /*****************************************************************************************************************************
     *
     * Public Methods
     *
     *****************************************************************************************************************************/

    /**
     * Retrieve application(s) by application id(s)
     *
     * @param   applicationIds  A list of application id(s)
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByApplicationIds(List<Id> applicationIds) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'Id IN :applicationIds'};
        
        orderBy.add(0,'application_round_year__c DESC');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }

    /**
     * Retrieve application(s) by applicant id(s)
     *
     * @param   applicantIds    A list of applicant id(s)
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByApplicantIds(List<Id> applicantIds) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'applicant_id__c IN :applicantIds'};
        
        orderBy.add(0,'application_round_year__c DESC');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }

    /**
     * Retrieve application(s) by user id(s)
     *
     * @param   userIds     A list of user id(s)
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByUserIds(List<Id> contactIds) {
        // Construct where clauses 
        //Commented out this line on 08/28/2019 as we are using standard User object instead of Custom Public User 
        //List<String> whereclauses = new List<String>{'applicant_id__r.public_user_id__c IN :userIds'};
        List<String> whereclauses = new List<String>{'Contact__c IN :contactIds'};
        
        orderBy.add(0,'application_round_year__c DESC');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }
    
    /**  PK ADM Home Page changes
     * Retrieve application(s) by user id(s) and ProgramTypes
     * @param   userIds     A list of user id(s) 
     * @param   programTypes A list of programType(s) - Optional - ignores if it is null or nothing in the list
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByUserIdsAndProgramTypes(List<Id> userIds, List<String> programTypes){
		return getApplicationByUserIdsAndProgramTypesAndYr( userIds, programTypes, null );
    }
    
    
    public List<adm_application__c> getApplicationByUserIdsAndProgramTypesAndYr(List<Id> userIds, List<String> programTypes, String year ) {
    	 return getApplicationByUserIdsAndProgramTypesYrAndStatus(userIds, programTypes, year, null, null);
    }
    
    
    public List<adm_application__c> getApplicationByUserIdsAndProgramTypesYrAndStatus(List<Id> conIds, List<String> programTypes, String year, 
    																					List<String> inStatus, List<String> notInStatus) {
    	// Construct where clauses
    	// Commented out this line on 08/28/2019 as we are using standard User object instead of Custom Public User
		// List<String> whereclauses = new List<String> {'applicant_id__r.public_user_id__c IN :userIds'};
        List<String> whereclauses = new List<String> {'Contact__c IN :conIds'};
        
		if( (programTypes != null)  && (!programTypes.isEmpty()) ){
			whereclauses.add('round_id__r.program_type__c IN :programTypes');
		}
		
		if (year != null){
			whereclauses.add('application_round_year__c = :year');
		}
		
		if( (inStatus != null)  && (!inStatus.isEmpty()) ){
			whereclauses.add('status__c IN :inStatus');
		}
		
		if( (notInStatus != null)  && (!notInStatus.isEmpty()) ){
			whereclauses.add('status__c NOT IN :notInStatus');
		}
		orderBy.add(0,'LastModifiedDate DESC');
		orderBy.add(0,'application_round_year__c DESC');
		
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql); 
    }   
    /**
     * Retrieve application(s) by Interviewer id(s)
     *
     * @param   interviewerIds     A list of interviewer id(s)
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByInterviewerIds(List<Id> interviewerIds) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'interviewer_id__c IN :interviewerIds OR supplemental_interviewer_id__c IN :interviewerIds'};
        
        orderBy.add(0,'application_round_year__c DESC');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }

    /**
     * Get application(s) for plagiarism score and document url update
     *
     * @param   roundId     A collection of round id(s)
     * @param   programCode Program code
     * @param   statuses    A collection of status(es)
     * @param   recLimit    Record limit
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationForPlagiarismUpdate(List<Id> roundIds, String programCode, List<String> statuses, Integer recLimit) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'plagiarism_document_id__c != null', 'plagiarism_document_url__c = null', 'plagiarism_score__c = null'};
        if (roundIds != null && !roundIds.isEmpty()) whereclauses.add('round_id__c IN :roundIds');
        if (!String.isBlank(programCode)) {
                programCode = programCode.toLowerCase();
                whereclauses.add('program_code__c = :programCode');
            }
        if (statuses != null && !statuses.isEmpty()) whereclauses.add('status__c IN :statuses');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null, recLimit == null ? 0 : recLimit);
        
        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);
    }

    /**
     * Retrieve application(s) by round and application status (lor pending and lor complete)
     *
     * @param   roundId     A collection of round id(s)
     * @param   statuses    A collection of status(es)
     *
     * @return  A collection of application(s)
     */
    public List<adm_application__c> getApplicationsByRound(List<Id> roundIds, List<String> statuses) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'round_id__c IN :roundIds'};
        if (statuses != null && !statuses.isEmpty()) whereclauses.add('status__c IN :statuses');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);
        
        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);
    }

     // PK  ADMHomePage Changes
     /**
     * Retrieve application(s) by round and application status for the user
     *
     * @param   userid  public user id
     * @param   roundIds   A collection of round id(s) Optional - if roundIds is null then it is ignored in whereclause 
     * @param   statuses    A collection of status(es) Optional - Ignored if null or length is zero
     * @param   isINstatues  true checks for in statues, false checks for Not In statues
     * @return  A collection of application(s)
     */
    public List<adm_application__c> getApplicationsByUserAndRounds(String conid, List<Id> roundIds, List<String> statuses, Boolean isINstatues) {
        // Construct where clauses
            List<String> whereclauses = new List<String>{'Contact__c = :conid'};
            
            if ( (roundIds != null) && (roundIds.size() > 0) ){
                whereclauses.add('round_id__c IN :roundIds');
            }

            if ( (statuses != null) && (statuses.size() > 0) ){
                if(isINstatues){
                 whereclauses.add('status__c IN :statuses');
                }
                else{
                 whereclauses.add('status__c NOT IN :statuses');
                }
            }
        
            // Build SOQL
            String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);
        
             // Query and return collection
            return (List<adm_application__c>) Database.query(soql); 
       
    }


    /**
     * Retrieve application(s) by round and application status (lor pending and lor complete)
     *
     * @param   roundId      A collection of round id(s)
     * @param   statuses     A collection of status(es)
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationsAndLookupFieldsByRound(List<Id> roundIds, List<String> statuses) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'round_id__c IN :roundIds'};
        if (statuses != null && !statuses.isEmpty()) whereclauses.add('status__c  IN :statuses');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationAndLookupFields, whereclauses, true, null);
        
        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);
    }

    /**
     * Get interviewed applications
     *
     * @param   notificationType        Interview bulk notification type
     * @param   programCode             Program code
     * @param   startingInterviewDate   Starting interview date (from round object)
     *
     * @return  A collection of interviewed application objects
     */
    public List<adm_application__c> getInterviewedApplications(ADMConstants.INTERVIEW_BULK_NOTIFICATION_TYPE notificationType, 
        String programCode, Datetime startingInterviewDate) {
        // Construct where clauses
        Datetime rightNow = System.now();
        Date todayDate = System.today();
        List<String> whereclauses;
		List<String> statuses;
        if(ADMConstants.PROGRAMCODE.MBA.name().equalsIgnoreCase(programCode)){
            whereclauses = new List<String>{'interview_timestamp__c != null', 'interview_timestamp__c >= :startingInterviewDate', 
            'interview_timestamp__c < :rightNow', 'is_interview__c = true'};
            if (notificationType == ADMConstants.INTERVIEW_BULK_NOTIFICATION_TYPE.ThankYou) whereclauses.add('post_interview_mail_timestamp__c = null');
            else if (notificationType == ADMConstants.INTERVIEW_BULK_NOTIFICATION_TYPE.FirstReminder) {
                whereclauses.add('no_evaluation__c = false');
                whereclauses.add('post_interview_mail_timestamp__c != null');
                whereclauses.add('first_reminder_mail_timestamp__c = null');
            }
            else if (notificationType == ADMConstants.INTERVIEW_BULK_NOTIFICATION_TYPE.SecondReminder) {
                // We don't want to send out second remoinder at the same time as the first reminder.
                // So we are checking to make sure that the first reminder mail timestamp is not today.
                whereclauses.add('no_evaluation__c = false');
                whereclauses.add('post_interview_mail_timestamp__c != null');
                whereclauses.add('first_reminder_mail_timestamp__c != null');
                whereclauses.add('first_reminder_mail_timestamp__c < :todayDate');
                whereclauses.add('second_reminder_mail_timestamp__c = null');
            }
            //ADM-1385, to send the interview match reminder email to applicant
            else if (notificationType == ADMConstants.INTERVIEW_BULK_NOTIFICATION_TYPE.ApplicantReminder) {
                whereclauses = new List<String>();
                statuses = new List<String>{ADMConstants.INTERVIEW_STATUS.Interview_Matched.name().replace(
                                    CoreConstants.DELIMITER_UNDERSCORE, CoreConstants.DELIMITER_SPACE)};
                
                whereclauses.add('mba_interview_status__c  IN :statuses');
                whereclauses.add('applicant_reminder_date__c = null');
            }
        }
        else if(ADMConstants.PROGRAMCODE.MSx.name().equalsIgnoreCase(programCode)){
            whereclauses = new List<String>{'msx_interview_timestamp__c != null', 'msx_interview_timestamp__c >= :startingInterviewDate', 
            'msx_interview_timestamp__c < :rightNow', 'is_msx_interview__c = true'};
            if (notificationType == ADMConstants.INTERVIEW_BULK_NOTIFICATION_TYPE.ThankYou) whereclauses.add('msx_post_interview_mail_timestamp__c = null');
            //ADM-1385, to send the interview match reminder email to applicant
            else if (notificationType == ADMConstants.INTERVIEW_BULK_NOTIFICATION_TYPE.ApplicantReminder) {
                whereclauses = new List<String>();
                statuses = new List<String>{ADMConstants.INTERVIEW_STATUS.Interview_Matched.name().replace(
                                    CoreConstants.DELIMITER_UNDERSCORE, CoreConstants.DELIMITER_SPACE)};
                
                whereclauses.add('msx_interview_status__c  IN :statuses');
                whereclauses.add('msx_applicant_reminder_date__c = null');
            }
        }
        
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);
        
        // Query and return collection
        List<adm_application__c> applicationObjs = new List<adm_application__c>();
        for (adm_application__c  app : (List<adm_application__c>) Database.query(soql)) {
            if (app.round_id__r.program_code__c != null && 
               (app.round_id__r.program_code__c.equalsIgnoreCase(programCode) || app.round_id__r.program_code__c.equalsIgnoreCase(ADMConstants.PROGRAMCODE.MBAMSx.name()))) 
                applicationObjs.add(app);
        }
        
        return applicationObjs;
    }

    /**
     * Retrieve application(s) by round and application status (lor pending and lor complete)
     *
     * @param   roundId      A collection of round id(s)
     * @param   statuses     A collection of status(es)
     *
     * @return  A query Locater
     */
    public Database.QueryLocator getQueryForApplicationsAndLookupFieldsByRound(List<Id> roundIds, List<String> statuses) {
        // Construct where clauses
        
        List<String> whereclauses = new List<String>{'round_id__c IN :roundIds'};
        if (statuses != null && !statuses.isEmpty()) whereclauses.add('status__c  IN :statuses');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationAndLookupFields, whereclauses, true, null);
        
        // Query and return collection
        return Database.getQueryLocator(soql);

    }
    
    
    /**
     * Retrieve application(s) by application id(s) and application status
     *
     * @param   applicationIds  A list of application id(s)
     * @param   statuses  A list of application status(s)
     * @param   programCode   Program Code of the round
     * @param   year   Year on the round object
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByApplicationIdsAndStatus(List<Id> applicationIds, List<String> statuses,String programCode, String year) {
        // Construct where clauses
        //List<String> whereclauses = new List<String>{'Id IN :applicationIds'};
        List<String> whereclauses = new List<String>();
        if(!applicationIds.isEmpty() && applicationIds!=null)whereclauses.add('Id IN :applicationIds');
        if (statuses != null && !statuses.isEmpty()) whereclauses.add('status__c  IN :statuses');
        if (!String.isBlank(programCode))  whereClauses.add('program_code__c = :programCode');
        if (!String.isBlank(year)) whereClauses.add('application_round_year__c = :year');
        
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }
    
    /**
     * Retrieve application(s) by gsb account id(s)
     *
     * @param   gsbAccountIds  A list of gsb account id(s)
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByGSBAccountId(List<Id> gsbAccountIds) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'applicant_id__r.gsb_account_id__c IN :gsbAccountIds'};
		List<String> orderByDate = new List<String> {'CreatedDate DESC'};
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, orderByDate);
		

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }
	
/**
     * Retrieve application(s) by waitpool and decision round ids
     *
     *  @param   waitpoolRoundIds  A list of waitpool round id(s)
     *  @param   decisionRoundIds  A list of decision round id(s)
     *  @param   waitpoolEqual  Boolean to specify whether to use in or not in in where clause for waitpool round ids
     *  @param   decisionEqual  Boolean to specify whether to use in or not in in where clause for decision round ids
     *  @param   programCode   Program Code of the round
     *  @param   year   Year on the round object
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByWaitpoolAndDecisionRound(List<Id> waitpoolRoundIds, List<Id> decisionRoundIds, Boolean waitpoolEqual, Boolean decisionEqual,String programCode, String year) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{};
        if(waitpoolEqual) whereClauses.add('waitpool_round_id__c in :waitpoolRoundIds');
        else whereClauses.add('waitpool_round_id__c not in  :waitpoolRoundIds');
        
        if(decisionEqual) whereClauses.add('decision_round_id__c in  :decisionRoundIds');
        else whereClauses.add('decision_round_id__c not in  :decisionRoundIds');
        
        
        if (!String.isBlank(programCode))  whereClauses.add('program_code__c = :programCode');
        if (!String.isBlank(year)) whereClauses.add('application_round_year__c = :year');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }
 /// PK
 /**
     * Retrieve application(s) by Applicant Ids
     *
     *  @param   applicantIds  A list of applicants id(s)
     *  @param   programTypes  A list of program type(s)
     *  @param   year  Application year
     *  @param   statuses
     *  @param   isINstatues - if the value is true then it checks statues IN , if false it checks for statues NOT IN
     * @return  A collection of  application(s)
     */
     public List<adm_application__c> getApplicationByApplicantIds(List<Id> applicantIds, List<String> programTypes, String year, List<String> statuses, Boolean isINstatues ) {
      //  List<String> whereclauses = new List<String>{'applicant_id__r.public_user_id__c = :userid'};
        List<String> whereclauses = new List<String>{'applicant_id__c IN :applicantIds'};
        
         if( (programTypes != null)  && (programTypes.size() > 0) ){
           whereclauses.add('round_id__r.program_type__c IN :programTypes');
         }

        if(year != null) whereClauses.add('application_round_year__c = :year');

        if ( (statuses != null) && (statuses.size() > 0) ){
                if(isINstatues){
                 whereclauses.add('status__c IN :statuses');
                }
                else{
                 whereclauses.add('status__c NOT IN :statuses');
                }
            }
      
         // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);
    }

    /**
     * Retrieve application(s)
     *
     * @param   applicationRoundYears       A collection of application round year(s)
     * @param   directorDecisionStatuses    A collection of director decision status(es)
     * @param   programCodes			    A collection of program codes
     *
     * @return  A query Locater
     */
    public Database.QueryLocator getQueryForApplicationFields(List<String> applicationRoundYears, List<String> directorDecisionStatuses, List<String> programCodes) {
        // Construct where clauses
        
        List<String> whereclauses = new List<String>();
        if (applicationRoundYears != null && !applicationRoundYears.isEmpty()) whereclauses.add('application_round_year__c IN :applicationRoundYears');
        if (directorDecisionStatuses != null && !directorDecisionStatuses.isEmpty()) whereclauses.add('director_decision__c  IN :directorDecisionStatuses');
        if (programCodes != null && !programCodes.isEmpty()) whereclauses.add('program_code__c IN :programCodes');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);
        
        // Query and return collection
        return Database.getQueryLocator(soql);

    }
	
	/**
     * Retrieve application(s)
     *
     * @param   decisionRounIds       A collection of decision round Ids
     * @param   statuses    A collection of  status(es)
     *
     * @return  List of applications
     */
	public List<adm_application__c> getApplicationsByDecisionRoundAndStatus(List<Id> decisionRounIds, List<String> statuses) {
        // Construct where clauses
        List<String> whereclauses = new List<String>();
        if(!decisionRounIds.isEmpty() && decisionRounIds!=null)whereclauses.add('decision_round_id__c IN :decisionRounIds');
        if (statuses != null && !statuses.isEmpty()) whereclauses.add('status__c  IN :statuses');

        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, null);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }

    /**
     * Retrieve application(s) by application Numbers
     *
     * @param   applicationNumbers  A list of application Numbers
     *
     * @return  A collection of  application(s)
     */
    public List<adm_application__c> getApplicationByApplicationNumbers(List<String> applicationNumbers) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'application_number__c IN :applicationNumbers'};
        
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_application__c', applicationFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_application__c>) Database.query(soql);

    }

    /*****************************************************************************************************************************
     *
     * Private Methods
     *
     *****************************************************************************************************************************/


}