/*
 * @author     Rekha Shivappa
 * @date       May 30, 2014
 * @decription Controller class for the Submit Help Request page
 */
public with sharing class ADMHelpRequestController extends ADMBasePageController {
    /*****************************************************************************************************************************
     *
     * Constants & Private Variables
     *
     *****************************************************************************************************************************/
    
    private Map<String, String> TM_TEXT_OBJ = new Map<String, String>();
    private Map<String, String> TM_ERROR_TEXT = new Map<String, String>();
    private Map<String, String> TM_SETTING_OBJ = new Map<String, String>();
    
    private String userRole;
    
    
    /*****************************************************************************************************************************
     *
     * Properties
     *
     *****************************************************************************************************************************/
    /**
     * @description - the selected value for the Application list
     */
    public String selectedAppSection { get; set; }
    
    /**
     * @description - value selected for the Action
     */
    public String selectedAppAction { get; set; }
    
    /**
     * @description - value selected for the Action
     */
    public String detailedDescription { get; set; }
             
    /**
     * @description applicationListOptions
     */
    public List<SelectOption> applicationListOptions {
        get { 
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption(CoreConstants.BLANK_STRING, CoreConstants.DEFAULT_ONE));
            try {
                String[] apps = this.TM_TEXT_OBJ.get(String.format(ADMConstants.TM_HELP_REQUEST_APPLICATION_SECTION_LIST, 
                    new List<String>{programCode, userRole})).split(CoreConstants.DELIMITER_COMMA);
                if (apps!=null) {
                    for (String appName : apps) {
                        options.add(new SelectOption(String.valueof(appName), appName));
                    }
                }                   
            } catch (Exception ex) {}
            return options;
        }
        set;
    } 
    
    
    /**
     * @description applicationListOptions
     */
    public List<SelectOption> actionListOptions {
        get { 
            
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption(CoreConstants.BLANK_STRING, CoreConstants.DEFAULT_ONE));
            try {
                String[] apps = this.TM_TEXT_OBJ.get(String.format(ADMConstants.TM_HELP_REQUEST_APPLICATION_ACTION_LIST, 
                    new List<String>{programCode, userRole})).split(CoreConstants.DELIMITER_COMMA);
                if (apps!=null) {
                    for (String appName : apps) {
                        options.add(new SelectOption(String.valueof(appName), appName));
                    }
                }                   
            } catch (Exception ex) {}
            return options;
        }
        set;
    }
    
    /**
     * @description TM - Text Manager content property
     */
    public Map<String, String> TMContent {
         get { return TM_TEXT_OBJ; }
    }
    
    /*****************************************************************************************************************************
     *
     * Constructors
     *
     *****************************************************************************************************************************/
    
    public ADMHelpRequestController() {
        super.initialize(); 
        
        // Get user role
        userRole = System.currentPagereference().getParameters().get(ADMConstants.URL_PARAM_NAME.prole.name());
       
        getTMSettingAndContent();
    }
    
    /*****************************************************************************************************************************
     *
     * Action Methods
     *
     *****************************************************************************************************************************/
    /**
     * Sends the notification on the Help request to the admin with the selected section name,action and description
     *
     * @return  Page reference object
     */
    
    public PageReference submitHelpRequest() {
        ADMNotificationService nService = new ADMNotificationService(this.programCode);
        Map<String, String> params = new Map<String, String>();
        params.put(ADMConstants.HELP_REQUEST_PARAMS.roleName.name(), CoreUtils.formatString(userRole));
        params.put(ADMConstants.HELP_REQUEST_PARAMS.sectionName.name(), CoreUtils.formatString(selectedAppSection));
        params.put(ADMConstants.HELP_REQUEST_PARAMS.actionName.name(), CoreUtils.formatString(selectedAppAction));
        params.put(ADMConstants.HELP_REQUEST_PARAMS.description.name(), CoreUtils.formatString(detailedDescription));
        if(ADMConstants.USER_ROLE.interviewer.name().equalsIgnoreCase(userRole)){
            params.put(ADMConstants.HELP_REQUEST_PARAMS.fromEmail.name(), admInterviewer.preferred_email_address__c);
            params.put(ADMConstants.HELP_REQUEST_PARAMS.fromUserName.name(), CoreUtils.formatString(admInterviewer.first_name__c)
                                            +CoreConstants.DELIMITER_SPACE+CoreUtils.formatString(admInterviewer.last_name__c));
        }else{
            //Commented to change custom Public user object to standard community user
            /*params.put(ADMConstants.HELP_REQUEST_PARAMS.fromEmail.name(), pUserAccountInfo.publicUser.email__c);
            params.put(ADMConstants.HELP_REQUEST_PARAMS.fromUserName.name(), CoreUtils.formatString(pUserAccountInfo.publicUser.first_name__c)
                                            +CoreConstants.DELIMITER_SPACE+CoreUtils.formatString(pUserAccountInfo.publicUser.last_name__c));*/
            User currentloggedinuser = [Select Id,firstname,lastname,email,ContactId from User where Id=:UserInfo.getUserId()];
            params.put(ADMConstants.HELP_REQUEST_PARAMS.fromEmail.name(), currentloggedinuser.email);
            params.put(ADMConstants.HELP_REQUEST_PARAMS.fromUserName.name(), CoreUtils.formatString(currentloggedinuser.firstname)
                                            +CoreConstants.DELIMITER_SPACE+CoreUtils.formatString(currentloggedinuser.lastname));
        }
        nService.helpRequestNotification(ADMNotificationService.APPLICATION_NOTIFICATION_TYPE.HelpRequest,  params);
        return null;    
    }
    
    /*****************************************************************************************************************************
     *
     * Private Methods
     *
     *****************************************************************************************************************************/
    /**
     * Retrieve both application settings and text manager content
     *
     * @param   programCode Program code
     *
     * @return  None
     */
    private void getTMSettingAndContent() {
        this.TM_TEXT_OBJ = TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_HELP_REQUEST_ALL, new List<String>{programCode}));
        TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_HELP_REQUEST_APPLICATION, new List<String>{programCode, userRole})));
    }    

 
   
}