/**
 * This class is the controller class for the Decision2 Post/Notify Report to display the list of 
 * applicants who are considered in Post/Notify jobs.
 *
 * @author  Rekha Shivappa
 * @date    June 25, 2016
 */

public with sharing class ADMDecisionPostNotifyReportController {
    private adm_round__c round = null;
    public List<adm_application__c> apps  {get;set;} 
    
    private static final  String DECISION_2_POST_ACTION_NAME = 'Decision2-Post';
    private static final  String DECISION_2_NOTIFY_ACTION_NAME = 'Decision2-Notify';
    private static final  String DECISION_1_POST_ACTION_NAME = 'Decision1-Post';
    private static final  String DECISION_1_NOTIFY_ACTION_NAME = 'Decision1-Notify';
    
    
    public ADMDecisionPostNotifyReportController (){}

    /**
     * This method is invoked from the ADMDecisionPostNotifyReport VF page-On click of the Decision 2Post/Notify Report
     * button on the Round object.  If the method is invoked from Decision2Post-Report, the applicants who are
     * considered for Post job is fetched. To fetch this we are using the same method prepareAppsForDecision2Post
     * from ADMBatchJobsService class and this is the same method which is used in Post job. 
     * Similarly when Notify Report button is clicked, the applicants who are
     * considered for the Notify job are fetched. Here again we use getAppsForDecision2Notify method from 
     * ADMBatchJobsService class and this is the same method which is used in Notify job as well.
     *
     *
     * @return  PageReference
     */    
    public PageReference decisionPostNotifyReport() {
        ADMBatchJobsService batchService = new ADMBatchJobsService();
        String roundId = System.currentPagereference().getParameters().get('roundId');
        String programCode = System.currentPagereference().getParameters().get('programCode');
        String batchName = System.currentPagereference().getParameters().get('batchName');
        
        //If requested Report is for Post
        if (batchName.equals(DECISION_2_POST_ACTION_NAME)) 
            apps= batchService.prepareAppsForDecision2Post(new List<Id>{roundId},programCode.toLowerCase());
        else if (batchName.equals(DECISION_2_NOTIFY_ACTION_NAME)) ////If requested Report is for Notify
            apps= batchService.getAppsForDecision2Notify(new List<Id>{roundId});
        else if (batchName.equals(DECISION_1_POST_ACTION_NAME)) {
            ADMBatchJobsService admBatchService = new ADMBatchJobsService(batchName, programCode.toLowerCase());
            apps= admBatchService.processDecision1PostApplication(new List<Id>{roundId},programCode.toLowerCase());
        }  else if (batchName.equals(DECISION_1_NOTIFY_ACTION_NAME)) ////If requested Report is for Notify
            apps= batchService.getApplicantsNotifiedAsPartOfDecisionPosting1Notification(new List<Id>{roundId}, programCode.toLowerCase()); 
        
        //Add sort column value and list of rows/records which are having same column value as key & value pairs to sortColumnMap 
        Map<object, List<object>> sortColumnMap = new  Map<object, List<object>>();
        for (adm_application__c appObj : apps) {
            //Commented as public User is changed to Standard Community User
            //String puserName = appObj.applicant_id__r.public_user_id__r.email__c;
            String puserName = appObj.Contact__r.email;
            if (puserName!=null) {
                sortColumnMap.put(puserName,new List<object>{appObj});
            }
        }
     
        this.apps.clear();
        for (object obj : CoreUtils.sortDataByColumn(sortColumnMap, true)) {
            this.apps.add((adm_application__c)obj);
        }

        return null;
    }

    
}