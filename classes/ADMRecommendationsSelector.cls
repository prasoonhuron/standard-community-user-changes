/*
 * @author      Chenji Srinivasulu
 * @date        April 22, 2014
 * @description Selector class for recommendations
 */
public with sharing class ADMRecommendationsSelector extends BaseSelector {
    /**
     * List of fields from ADM Recommendations object
     */
    private final List<String> recommendationsFields = new List<String>{'Id', 'Name', 'application_id__c', 
        'city__c',  'country_id__c', 'country_code__c', 'country_id__r.country__c','date_notified__c', 'email_address__c', 'employer__c', 'extension__c', 'first_name__c',  
        'job_title__c', 'last_name__c', 'phone__c',  'phone_type__c', 'relationship_to_you__c', 'personal_note_to_recommender__c', 'is_account_created__c',
        'reminder_date_notified__c', 'waived_right_to_see_recommendation__c', 'remind_recommender__c', 'recommender_status__c','call_work_phone__c', 'comment_on_the_ratings__c',
        'change_leadership__c','communicating__c','context_of_interaction__c','developing_others__c','digital_signature__c', 'reminder_timestamp__c', 'describe_this_candidate__c',
        'direct_supervisor__c','influence_and_collaboration__c','information_seeking__c','know_applicant_for_months__c','translator_country_code__c','translator_extension__c',
        'know_applicant_for_years__c','most_freq_applicant_cont_to_internal__c','most_freq_applicant_cont_from_internal__c','initiative__c','adaptability_resilience__c','self_awareness__c','problem_solving__c', 
        'most_frequent_applicant_contact_from__c', 'most_frequent_applicant_contact_to__c','overall__c','peer_group_comparison__c','referee_in_context_of__c', 
        'required_translation_to_interact__c', 'respect_for_others__c','results_orientation__c','signature_date__c','reference_questions_attachment_id__c', 
        'signature_date_internal__c','strategic_orientation__c','team_leadership__c','translator_name__c','translator_phone__c','trustworthiness__c', 
        'public_user_id__c', 'public_user_id__r.email__c', 'public_user_id__r.user_name__c', 'application_id__r.applicant_id__r.first_name__c', 'application_id__r.applicant_id__r.name',
        'application_id__r.applicant_id__r.last_name__c', 'application_id__r.status__c','stanford_mba_class_year__c' ,'no_of_applicants_recommended_this_year__c','application_id__r.round_id__r.Name',
        'application_id__r.round_id__c','application_id__r.round_id__r.end_date_time__c','application_id__r.high_school_name__c','application_id__r.applicant_id__r.public_user_id__r.email__c' , 'know_recommender__c', 'designated_for_msx__c',
        'mailing_state_id__c', 'mailing_street_1__c', 'mailing_street_2__c', 'mailing_zip__c', 'application_id__r.program_code__c',
        'mailing_state_id__r.state__c', 'application_id__r.round_id__r.year__c', 'application_id__r.applicant_id__r.email__c','public_user_id__r.password__c'};
    
 
        
        
    private final List<String> recommendationsCountField = new List<String>{'count(Id) numberOfRecomendations'}; //'
    
    /**
     * Order by clause
     */
    private final List<String> orderBy = new List<String> {'CreatedDate ASC', 'first_name__c'};       

    /*****************************************************************************************************************************
     *
     * Constructors
     *
     *****************************************************************************************************************************/
    public ADMRecommendationsSelector() {}

    /*****************************************************************************************************************************
     *
     * Public Methods
     *
     *****************************************************************************************************************************/

     /**
     * Retrieve Recommendations by application(s)
     *
     * @param   applicationIds  A list of application id(s)
     *
     * @return  A collection of adm_recommendation__c
     */
    public List<adm_recommendation__c> getRecommendationsByApplicationIds(List<Id> appplicationIds) {
        return getRecommendationsByApplicationIds(appplicationIds, null, false);
    }

     /**
     * Retrieve Recommendations by application(s) with given statuses
     *
     * @param   applicationIds  A list of application id(s)
     * @param   statuses        List of recommendation statuses
     * @param   isNotStatuses   A boolean indicating if to use NOT IN/IN statuses
     *
     * @return  A collection of adm_recommendation__c
     */
    public List<adm_recommendation__c> getRecommendationsByApplicationIds(List<Id> appplicationIds, List<String> statuses, Boolean isNotStatuses) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'application_id__c IN :appplicationIds'};
        if (statuses != null && !statuses.isEmpty()) {
            if (isNotStatuses) whereclauses.add('recommender_status__c NOT IN :statuses');
            else whereclauses.add('recommender_status__c IN :statuses');
        }
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_recommendation__c', recommendationsFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_recommendation__c>) Database.query(soql);
    }
    
   /**
    *   @description    Get All Unsubmitted Recommendation Record.
    *
    *   @param          Recommendation Status
    *
    *   @return Integer adm_recommendation__c
    */    
    
    public Database.QueryLocator getRecommendations(List<Id> roundIds,  List<String> statuses){
        // Construct where clauses
         List<String> whereclauses = new List<String>{'application_id__r.round_id__c IN :roundIds', 'reminder_timestamp__c = null', 'remind_recommender__c=true', 'public_user_id__r.email__c!=null'};
         if (!statuses.isEmpty()) whereclauses.add('recommender_status__c NOT IN :statuses');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_recommendation__c', recommendationsFields, whereclauses, true, null);
        
        // Query and return Count
        return Database.getQueryLocator(soql);
    }
    
    
    /**
     * Retrieve Recommendations by Recommendation Ids
     *
     * @param   recommendationIds  A list of application id(s)
     * 
     *
     * @return  A collection of adm_recommendation__c
     */
    public List<adm_recommendation__c> getRecommendationsByIds(List<Id> recommendationIds) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'Id IN :recommendationIds'};
       
        // Build SOQL
        String soql = super.soqlBuilder('adm_recommendation__c', recommendationsFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_recommendation__c>) Database.query(soql);
    }
        
    /**
     * Retrieve Recommendations by email(s)
     *
     * @param   emailList  A list of email(s)
     *
     * @return  A collection of adm_recommendation__c
     */
    public List<adm_recommendation__c> getRecommendationsByEmails(List<String> emailList) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'email_address__c IN :emailList'};
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_recommendation__c', recommendationsFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_recommendation__c>) Database.query(soql);
    }

    /**
     * Retrieve Recommender application(s)
     *
     * @param   publicUserIds  A list of public user id(s)
     * @param   statuses       A list of recommendation statuse(s)
     *
     * @return  A collection of adm_recommendation__c
     */
    public List<adm_recommendation__c> getRecommenderApplications(List<Id> publicUserIds, List<String> statuses) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'public_user_id__c IN :publicUserIds'};
        if (!statuses.isEmpty()) whereclauses.add('recommender_status__c IN :statuses');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_recommendation__c', recommendationsFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_recommendation__c>) Database.query(soql);
    }
    

    /**
     * Retrieve Recommender application(s)
     *
     * @param   publicUserIds                A list of public user id(s)
     * @param   statuses                     A list of recommendation statuse(s)
     * @param   setOfApplicationStatus       A set of application statuse(s)
     * @return  A collection of adm_recommendation__c
     */
    public List<adm_recommendation__c> getRecommenderApplicationsBasedOnStatus(List<Id> contactIds, List<String> statuses, Set<String> setOfApplicationStatus) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'application_id__r.status__c IN :setOfApplicationStatus','application_id__r.Contact__c IN : contactIds'};
        if (!statuses.isEmpty()) whereclauses.add('recommender_status__c IN :statuses');
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_recommendation__c', recommendationsFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_recommendation__c>) Database.query(soql);
    }
    
    /**
    *   @description    number of recommendations of a user.
    *
    *   @param  puId    Public user id
    *
    *   @return Integer Number of recommendations
    */    
    public Integer numberOfRecomendationsByPublicUserId(String conId){
        // Construct where clauses
        //List<String> whereclauses = new List<String>{'public_user_id__c = :puId'};
        List<String> whereclauses = new List<String>{'application_id__r.Contact__c = :conId'};
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_recommendation__c', recommendationsCountField, whereclauses, true, null);
        
        // Query and return Count
        return Integer.valueOf((Database.query(soql))[0].get('numberOfRecomendations'));
    }
    
    
    /*****************************************************************************************************************************
     *
     * Private Methods
     *
     *****************************************************************************************************************************/
}