/**
 * @author     Ranjith Kumar
 * @date       May 3, 2014
 * @decription Controller class for admissions dashboard section 
 */
public with sharing class ADMApplicationDashboardController extends ADMBasePageController {
    /*****************************************************************************************************************************
     *
     * Constants & Private Variables
     *
     *****************************************************************************************************************************/
    private Map<String, String> TM_TEXT_OBJ = new Map<String, String>();
    private Map<String, String> TM_SETTING_OBJ = new Map<String, String>();
    private String FIELD_SUFFIX = '__c';

    /*****************************************************************************************************************************
     *
     * Properties
     *
     *****************************************************************************************************************************/
    /**
     * @description ADM Employment collection
     */
    public List<ADMApplicationDashboardService.DashboardVO> admDashboardErrorList { get; set; }    
    
    /**
     * @description TM - Text Manager content property
     */
    public Map<String, String> TMContent { get { return TM_TEXT_OBJ; }}
    
    /*****************************************************************************************************************************
     *
     * Constructors
     *
     *****************************************************************************************************************************/
    public ADMApplicationDashboardController() {
        System.Debug('<<<<ADMApplicationDashboardControllerEntered>>>>');
        initialize();
        
        getTMSettingAndContent(this.programCode);
    }       
    
    /*****************************************************************************************************************************
     *
     * Action Methods
     *
     *****************************************************************************************************************************/

    /**
     * Initializes the awards and honors section
     *
     * @return  None
     */
    public override void initialize() {
        // Call parent controller initialization routine
        super.initialize();
        
        if(this.admApplication != null) {
            ADMExceptionService expService = new ADMExceptionService();
            Map<Id, Integer> exceptionCountMap = expService.getAggregateErrorsCount(new List<Id>{this.admApplication.Id}, 
                new List<ADMExceptionService.AGGREGATE_FIELD>{ADMExceptionService.AGGREGATE_FIELD.application_id, 
                ADMExceptionService.AGGREGATE_FIELD.navigation_section_id}, false).get(this.admApplication.Id);
            //Return map of exceptions for every navigation section.
            //TODO  If needed do the fetching only for certain stages of application and not all.
            Map<Id, List<adm_application_exception__c>> exceptionMessagesMap = expService.getErrorMessages(new List<Id>{this.admApplication.Id},new List<ADMExceptionService.AGGREGATE_FIELD>{ADMExceptionService.AGGREGATE_FIELD.application_id, 
                                                          ADMExceptionService.AGGREGATE_FIELD.navigation_section_id,ADMExceptionService.AGGREGATE_FIELD.message}, false).get(this.admApplication.Id);
           
            // Need to sort because the Map won't retain the sort returned by SOQL
            ADMNavigationService navigationService = new ADMNavigationService();
            //Return map of navigation sections based on logged in user role.
            Map<Id, adm_navigation_section__c> admNavigationSectionMap = navigationService.getNavigationSections('', ADMConstants.USER_ROLE.applicant.name(), '');
            
            ADMApplicationDashboardService dashboardService = new ADMApplicationDashboardService();
            this.admDashboardErrorList = dashboardService.getDashboardVOList(exceptionCountMap, getSortedNavigationSectionList(admNavigationSectionMap), exceptionMessagesMap);
        }
    }
    
   
    /**
     * Save method - Nothing to Save but this method is needed for left nav to work 
     *
     * @return  Page reference object
     */
    public override PageReference save() {
        // Check if to create essential objects
        PageReference pageref = super.save();
         if(pageRef != null)
             return pageRef;
        return CoreUtils.redirectPage(this.navigationUrl, super.createUrlParameters());
    } 

    /**
     * Submit application
     *
     * @return  Page reference object
     */
    public override PageReference submit() {
        return super.submit();
    }
    
   /**
    * Create application if the application does not exist & create applicant if applicant record does not exist.
    * Also assign next open round to the application if current round is inactive.  
    * This method will be invoked on dashboard page load.
    *
    * @return  None
    */
    public PageReference initializeADMApplicantApplicationWithNextOpenRound(){
        ADMRoundService admRoundService = new ADMRoundService();
        adm_round__c nextOpenRound = null;
        
        // If no current round or if current round presents but no next open round then applicant should be redirected to the home page.
        PageReference homePage = new PageReference(this.TM_SETTING_OBJ.get
                            (String.format(CoreConstants.TM_PUBLIC_USER_DEFAULT_HOME_PAGE, new List<String>{programCode})));
                            
        // Proceed only if the there is a current round (because there is a dependency on Round.year field to get the next open round)
        if(this.admCurrentRound != null && !String.isEmpty(this.admCurrentRound.Id)){
            // create Applicant record if it doesn't exist already
            User currentloggedinuser = [Select Id,Name,firstName,lastName,email,ContactId from User where Id=:UserInfo.getUserId()];
            //super.createApplicantIfApplicantDoesNotExist(this.admApplicant, this.pUserAccountInfo, this.admCurrentRound);
            super.createApplicantIfApplicantDoesNotExist(this.admApplicant, currentloggedinuser, this.admCurrentRound);
            // create Application record if it doesn't exist already
            super.createApplicationIfApplicationDoesNotExist(this.admApplication, this.admApplicant, this.admCurrentRound);
            
            // Get the next open round (If there is a current open active round it'll get that instead of 
            // next open round else it'll get the next open round)
            try {
                nextOpenRound = admRoundService.getNextOpenRound(this.admCurrentRound.year__c, true, this.admCurrentRound.program_code__c.toLowerCase());
            } catch(ADMExceptions.NoRoundException ex) {
                // If both current and next round is not available forward the user to home page
                return homePage;
            }
            
            // Round assignment for already existing application but the current round ended - Assign next open round
            if((nextOpenRound != null && !String.isEmpty(nextOpenRound.Id)) &&
                    (this.admApplication.status__c == ADMConstants.APPLICATION_STATUS.Started.name() || 
                            this.admApplication.status__c == ADMConstants.APPLICATION_STATUS.Pending.name() ||
                            this.admApplication.status__c == ADMConstants.APPLICATION_STATUS.Unsubmitted.name()) &&
                    this.admApplication.round_id__r.end_date_time__c <= Datetime.now()) {
                        
                // Set application status to Started
                this.admApplication.status__c = ADMConstants.APPLICATION_STATUS.Started.name();
                // Set application roung to next open round
                this.admApplication.round_id__c = nextOpenRound.Id;
                update admApplication;
            }
        } else {
            // If globally there is no current round return the user to home page
            return homePage;
        }
        return null;
    }       

    /*****************************************************************************************************************************
     *
     * Private Methods
     *
     *****************************************************************************************************************************/
    /**
     * Sort the Map and return the list with sorted order
     *
     * @param   admNavigationSectionMap Navigation Section map
     *
     * @return  List of Navigation sections with exceptions sorted by sequence
     */
    private List<adm_navigation_section__c> getSortedNavigationSectionList(Map<Id, adm_navigation_section__c> admNavigationSectionMap) {
        //First we'll need a map to store the sequency and the navigation section that sequence is associated with
        Map<Decimal, adm_navigation_section__c> navigationSectionSequenceMap = new Map<Decimal, adm_navigation_section__c>();
        for(adm_navigation_section__c navigationSection : admNavigationSectionMap.values()) {
            //Adding program code to prevent overwriting of sections as there are same section names for both fellowship and admissions    
            if(this.programCode.equalsIgnoreCase(navigationSection.program_code__c))
                navigationSectionSequenceMap.put(navigationSection.sequence__c, navigationSection);
        }

        //Add these sequence in a list and sort
        List<Decimal> navigationSectionSequenceList = new List<Decimal>();
        navigationSectionSequenceList.addAll(navigationSectionSequenceMap.keySet());
        navigationSectionSequenceList.sort();

        //Based on the sorted sequence prepare a map with the original sorted data
        List<adm_navigation_section__c> sortedNavigationSectionList = new List<adm_navigation_section__c>();
        for(Decimal sequence : navigationSectionSequenceList) {
            sortedNavigationSectionList.add(navigationSectionSequenceMap.get(sequence));
        }

        return sortedNavigationSectionList;
    }
    
    /**
     * Retrieve both application settings and text manager content
     *
     * @param   programCode Program code
     *
     * @return  None
     */
    private void getTMSettingAndContent(String programCode) {
        // Get page instructions        
        this.TM_TEXT_OBJ = TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_APPLICATION_DASHBOARD, new List<String>{programCode}));
        
        // Get application settings
        this.TM_SETTING_OBJ = TextManagerUtils.getAppSettingAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(CoreConstants.TM_PUBLIC_USER_DEFAULT_HOME_PAGE, new List<String>{programCode}));
    }     
}