/*
 * @author     KC Kwong
 * @date       February 21, 2014
 * @decription Selector class for applicant
 */
public with sharing class ADMApplicantSelector extends BaseSelector {
    
    /**
     * List of fields from ADM Application object
     */
    private final List<String> applicantFields = new List<String>{'Id', 'Contact__r.email','Name', 'first_name__c', 'last_name__c', 'middle_name__c ', 'suffix__c',
                                'preferred_name__c','name_change__c', 'previous_last_name__c', 'previous_first_name__c', 'previous_middle_name__c',
                                'previous_suffix__c', 'social_security_number__c', 'gender__c', 'gender_identity__c', 'birthdate__c','title__c', 'birth_country_id__c', 
                                'birth_state_id__c', 'birth_city__c', 'public_user_id__r.first_name__c', 'public_user_id__r.middle_name__c', 'public_user_id__r.last_name__c',
                                'public_user_id__r.email__c', 'public_user_id__r.user_name__c','birth_country_id__r.country__c','birth_state_id__r.state__c',
                                'gsb_account_id__r.contact_id__c', 'univid__c', 'gsb_account_id__c', 'primary_citizenship__r.country__c', 
                                'gsb_account_id__r.first_name__c','gsb_account_id__r.last_name__c','gsb_account_id__r.email__c',
                                'country_id__c', 'country_id__r.country__c', 'city__c', 'adm_city_id__c', 'state_name__c','stage__c','gsb_program_considering__c', 'gdpr_consent__c'};
   
    /**
     * List of fields from ADM Registration Survey  
     */     
    private final List<String> surveyFields = new List<String>{'Id', 'academic_options__c','adm_city_id__c','city__c','country_id__c','primary_citizenship__c','industry_category_id__c',
                                                    'email__c','ethnicity_1__c','ethnicity_2__c','ethnicity_3__c','first_name__c', 'gender__c','gender_identity__c','hear_stanford_mba_program__c','last_name__c',
                                                    'mba_enrollment_year__c', 'middle_name__c','public_user_id__c','stage__c','specify_other__c','country_id__r.country__c',
                                                    'specify_web__c','topics_of_interest__c','undergraduate_graduation_year__c', 'zip_postal_code__c','primary_citizenship__r.country__c',
                                                    'state_name__c','prospect_email__c','gsb_program_considering__c', 'areas_of_interest__c', 'state_id__c', 'gdpr_consent__c', 'communities_of_interest__c'};
        
    /**
     * Order by clause
     */
    private final List<String> orderBy = new List<String> {'CreatedDate'};
    /*****************************************************************************************************************************
     *
     * Constructors
     *
     *****************************************************************************************************************************/
    public ADMApplicantSelector() {}

    /*****************************************************************************************************************************
     *
     * Public Methods
     *
     *****************************************************************************************************************************/
    /**
     * Retrieve application(s) by application id(s)
     *
     * @param   applicationIds  A list of application id(s)
     *
     * @return  A collection of  application(s)
     */
    public List<adm_applicant__c> getApplicantByApplicantIds(List<Id> appplicantIds) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'Id IN :appplicantIds'};
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_applicant__c', applicantFields, whereclauses, true, orderBy);

        // Query and return collection
        return (List<adm_applicant__c>) Database.query(soql);

    }
    
    /**
     * Retrieve appplicant information by use id(s)
     *
     * @param   userIds A list of user id(s)
     *
     * @return  A collection of applicant information
     */
    public List<adm_applicant__c> getApplicantByUserIds(List<Id> contactIds) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'Contact__c IN :contactIds'};
        
        // Build SOQL
        String soql = super.soqlBuilder('adm_applicant__c', applicantFields, whereclauses, true, null);

        // Query and return collection
        return (List<adm_applicant__c>) Database.query(soql);
    }
    
    /**
     * Retrieve appplicant information by SUId(s)
     *
     * @param   suIds A list of SUId(s)
     *
     * @return  A collection of applicant information
     */
    public List<adm_applicant__c> getApplicantsBySuIds(List<String> suIds, Boolean nullAccountId) {
        // Construct where clauses
        List<String> whereclauses = new List<String>{'univid__c IN :suIds'};
        if(nullAccountId) whereclauses.add('gsb_account_id__c = null');
        // Build SOQL
        String soql = super.soqlBuilder('adm_applicant__c', applicantFields, whereclauses, true, null);

        // Query and return collection
        return (List<adm_applicant__c>) Database.query(soql);
    }
    
    /**
     * Gets an existing survey record based on email address
     *
     *
     * @return  Survey Record
     */
    public List<adm_applicant__c> getExistingSurvey(String userEmail) {       
        List<String> whereclauses = new List<String>{'prospect_email__c = :userEmail', 'email__c = :userEmail '};      
        String soql = super.soqlBuilder('adm_applicant__c', surveyFields, whereclauses, false, orderBy);
        
        // Query and return collection
        return (List<adm_applicant__c>) Database.query(soql);
    }
    
    
    /**
     * Gets an existing applicat record based on prospect email address
     *
     *
     * @return  List<adm_applicant__c> list of adm_applicant__c record
     */
    public List<adm_applicant__c> getApplicantByProspectEmail(String prospectEmail) {       
        List<String> whereclauses = new List<String>{'prospect_email__c = :prospectEmail'};      
        String soql = super.soqlBuilder('adm_applicant__c', applicantFields, whereclauses, false, orderBy);
        
        // Query and return collection
        return (List<adm_applicant__c>) Database.query(soql);
    }
    /*****************************************************************************************************************************
     *
     * Private Methods
     *
     *****************************************************************************************************************************/


}