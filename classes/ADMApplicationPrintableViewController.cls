/**
 * @description Controller class for admissions application printable view page
 * @author      Misha Verhovski    
 * @date        May 7, 2014
 */
public with sharing class ADMApplicationPrintableViewController extends ADMBasePageController{
    
/*****************************************************************************************************************************
 *
 * Constants & Private Variables
 *
 *****************************************************************************************************************************/

    private Map<String, String> TM_TEXT_OBJ = new Map<String, String>();
    private Map<String, String> TM_ERROR_TEXT = new Map<String, String>();
    private Map<String, String> TM_SETTING_OBJ = new Map<String, String>();

/*****************************************************************************************************************************
 *
 * Properties
 * 
 *****************************************************************************************************************************/
    
    /**
     * @description ADM ApplicationInformationVO object
     */
    public ADMApplicationService.ApplicationInformationVO applicationInformation { get; set; }
    
    /**
     * @description ADM TestResultsVO object
     */
    public ADMTestResultsService.TestResultsVO admTestResults { get; set; }

    /**
     * @description ADM AwardsAndHonorsVO object
     */
    public ADMAwardsAndHonorsService.AwardsAndHonorsVO admAwardsAndHonors { get; set; }

    /**
     * @description ADM AwardsAndHonorsVO collection
     */
    public List<ADMAwardsAndHonorsService.AwardsAndHonorsVO> admCurrentAwardsAndHonors { get; set; }

    /**
     * @description ADM Employment object
     */
    public ADMEmploymentService.EmploymentVO admEmployment { get; set; }
    
    /**
     * @description ADM Employment collection
     */
    public List<ADMEmploymentService.EmploymentVO> admCurrentEmployments { get; set; }
    
     /**
     * @description List of countries need states
     */
    public String listCountriesneedstates {
        get { 
            return this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_COUNTRIES_NEED_STATES, 
            new List<String>{this.programCode}));
        }
        set;
    }
    
    /**
     * @description Application Employment information
     */
    public ADMApplicationService.EmploymentInformationVO employmentInformation { get; set; }
    
    /**
     * @description 
     */
    public ADMEducationService.CollegeVO admCollege { get; set; }

    /**
     * @description 
     */
    public List<ADMEducationService.CollegeVO> admCurrentColleges { get; set; }
    
    /**
     * @description 
     */
    public ADMEducationService.LanguageVO admLanguage { get; set; }
    
    /**
     * @description 
     */
    public List<ADMEducationService.LanguageVO> admCurrentLanguages { get; set; }
    
    /**
     * @description Application Education information
     *              Education related information stored in the application object
     */
    public ADMApplicationService.EducationInformationVO educationInformation { get; set; }

    /**
     * @description ADM Family object
     */
    public ADMFamilyService.FamilyVO admFamily { get; set; }    
    
    /**
     * @description ADM Children object
     */
    public ADMFamilyService.ChildrenVO admChildren { get; set; }
    
    /**
     * @description ADM FamilyMember object
     */
    public ADMFamilyService.GSBConnectionVO admGSBConnection { get; set; }
    
    /**
     * @description ADM Spouse object
     */
    public ADMFamilyService.SpouseVO admSpouse { get; set; }
    
    /**
     * @description ADM ParentsAndGuardiansVO collection
     */
    public List<ADMFamilyService.FamilyVO> admCurrentParents { get; set; }
    
    /**
     * @description ADM ChildrenVO collection
     */
    public List<ADMFamilyService.ChildrenVO> admCurrentChildren { get; set; }
    
    /**
     * @description ADM GSBConnectionVO collection
     */
    public List<ADMFamilyService.GSBConnectionVO> admCurrentGSBConnections { get; set; }
    
    /**
     * @description Application Personal information
     *  Personal related information stored in the application object
     */
    public ADMApplicationService.PersonalInformationVO personalInformation { get; set; }
    
    /**
     * @description Applicant Personal information
     *  Personal related information stored in the applicant object
     */
    public ADMApplicantService.PersonalInformationVO applicantPersonalInformation { get; set; }

    /**
     * @description ADM Activities object
     */
    public ADMActivityService.ActivityVO admActivity { get; set; }
    
    /**
     * @description ADM Activites collection for displaying already existing activities
     */
    public List<ADMActivityService.ActivityVO> admCurrentActivities { get; set; }
    public List<ADMActivityService.ActivityVO> admActivities { get; set; }
    
    /**
     * @description Application Activities information
     * There are some pieces of information stored in the application object
     */
    public ADMApplicationService.ActivityInformationVO activityInformation { get; set; }
    
    /**
     * @description Application Essay
     *              There are some pieces of information stored in the application object
     */
    public ADMApplicationService.EssayVO essayVO { get; set; }
    
    /**
     * @description Application Additional information
     *              There are some pieces of information stored in the application object
     */
    public ADMApplicationService.AdditionalInformationVO additionalInformation { get; set; }
    
    /**
     * @description ADM RecommendationVO object
     */
     public ADMRecommendationsService.RecommendationVO recommendation { get; set; }

    /**
     * @description ADM RecommendationVO collection
     */
     public List<ADMRecommendationsService.RecommendationVO> admCurrentRecommendations { get; set; }
    
    /**
     * @description Board Exams
     *              There are some pieces of information stored in the application object
     */
    public ADMApplicationService.BoardExamsVO boardexamInformation { get; set; }

    /**
     * @description Financial Information
     *              There are some pieces of information stored in the application object
     */
    public ADMApplicationService.FinInfoResultsVO financialInformation { get; set; }

    /**
     * @description Countries property
     */
     public List<SelectOption> countries { 
        get { 
            return CoreUtils.getCountries(); 
        } 
     }
    
    /**
     * @description TM - Text Manager content property
     */
     
    public Map<String, String> TMContent { get { return TM_TEXT_OBJ; }}

    /**
     * @description Maximum colleges property
     */
    
    public Integer maximumColleges {
        get {
            Integer maxColleges = ADMConstants.DEFAULT_MAX_COLLEGES;
            try {
                maxColleges = Integer.valueOf(this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_MAXIMUM_COLLEGES, 
                    new List<String>{programCode})));
            }
            catch (Exception ex) {}
            return maxColleges;
        }
    }
    
    /**
     * @description Maximum languages property
     */
    public Integer maximumLanguages {
        get {
            Integer maxLanguages = ADMConstants.DEFAULT_MAX_LANGUAGES;
            try {
                maxLanguages = Integer.valueOf(this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_MAXIMUM_LANGUAGES, 
                    new List<String>{programCode})));
            }
            catch (Exception ex) {}
            return maxLanguages;
        }
    }
    
    /**
     * Applicant status to display in PDF. The process of generating PDF will invoke before Trigger executes, due to that adding this property.
     */
    public String applicantStatusForPDF {
        get {
            String status = ADMConstants.APPLICATION_STATUS.Submitted.name();
            if (applicationInformation.application.status__c.equalsIgnoreCase(ADMConstants.APPLICATION_STATUS.Submitted_LOR_Pending.name().replace(CoreConstants.DELIMITER_UNDERSCORE, CoreConstants.DELIMITER_SPACE))
                    || applicationInformation.application.status__c.equalsIgnoreCase(ADMConstants.APPLICATION_STATUS.Submitted_LOR_Complete.name().replace(CoreConstants.DELIMITER_UNDERSCORE, CoreConstants.DELIMITER_SPACE))
                    || applicationInformation.application.status__c.equalsIgnoreCase(ADMConstants.APPLICATION_STATUS.Submitted.name().replace(CoreConstants.DELIMITER_UNDERSCORE, CoreConstants.DELIMITER_SPACE))) {
                status = ADMConstants.APPLICATION_STATUS.Submitted.name();          
            }
            return status;
        }
    }
    

/*****************************************************************************************************************************
 *
 * Constructor
 *
 *****************************************************************************************************************************/
 
    public ADMApplicationPrintableViewController() {
        initialize();
        getTMSettingAndContent(this.programCode);
        initializePrintableView();
    }

    /**
     * Call parent controller initialization routine
     *
     * @return  None
     */
    public override void initialize() {
        // This URL parameter will be available during PDF generation.
        // In order to create UserAccount the user name is invoked from "Cookie", but this request(PDF generation) is an "Inline" pagareference
        // and Cookies will not be available, due to that we should not invoke invoke ADMBasePageController inititalize method during this action.
        if (System.currentPagereference().getParameters().get(ADMConstants.PDF_GENERATION_URL_PARAM_NAME.ispdfgeneration.name()) != null) {
            pdfInitialization();
        } 
        else super.initialize();
    }

/*****************************************************************************************************************************
 *
 * Private Methods
 *
 *****************************************************************************************************************************/
 
    private void pdfInitialization() {
        this.programCode = System.currentPagereference().getParameters().get(ADMConstants.URL_PARAM_NAME.program.name());
        this.applicationId = CoreUtils.decryptString(System.currentPagereference().getParameters().get(
                ADMConstants.URL_PARAM_NAME.pappid.name()));
        ADMApplicationService applicationService = new ADMApplicationService(this.programCode);
        List<adm_application__c> applications;
        applications = applicationService.getApplicationByApplicationIds(new List<Id>{this.applicationId});
        this.admApplication = applications[0]; 
        ADMApplicantService applicantService = new ADMApplicantService();
        //Commented out as Public User is changed to standard Community User 
        //List<adm_applicant__c> applicants = applicantService.getApplicantByUserIds(new List<Id>{this.admApplication.applicant_id__r.public_user_id__c});
        List<adm_applicant__c> applicants = applicantService.getApplicantByUserIds(new List<Id>{this.admApplication.Contact__c});
        if (applicants != null && !applicants.isEmpty()) this.admApplicant = applicants[0];
        this.pageFieldSettings = new GSBApplicationPageConfiguration(this.programCode, 
                    new List<String>{
                            CoreUtils.retrievePageName(ApexPages.CurrentPage()),
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMEssays.name(), ADMConstants.ADM_HOST_PAGE_NAMES.ADMApplicationInformation.name(), 
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMPersonalFamilyInformation.name(),
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMEmploymentInformation.name(),
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMEducationInformation.name()
                    });
        this.FIELD_SECTION_SETTINGS_MAP = pageFieldSettings.objectFieldSettingsMap;
        this.vfPageRequiredFields = pageFieldSettings.getRequiredFields();
        this.vfPageVisibleFields = pageFieldSettings.getVisibleFields();       
    }

    /**
     * Retrieve both application settings
     * @param   programCode Program code
     * @return  None
     */
    private void getTMSettingAndContent(String programCode) {

        // Get page instructions and common user setting identifiers
        this.TM_TEXT_OBJ = TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_EMPLOYMENT, new List<String>{programCode}));
        // app information
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
                String.format(ADMConstants.TM_APP_APPLICATION_INFORMATION, new List<String>{programCode})));
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_EDUCATION, new List<String>{programCode})));
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_ALL, new List<String>{programCode})));
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(),          
                                                      new String[]{String.format(ADMConstants.TM_APP_FAMILY_PARENT_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_PARENT_SUB_HEADER, new List<String>{programCode}),                                                          
                                                                   String.format(ADMConstants.TM_APP_FAMILY_OTHER_APPLICANT_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_CHILDREN_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_ROUND_HIGHLIGHT, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_SPOUSE_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_NAME_CAPITALIZATION_HEADER_INFO, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_PREVIOUS_NAME_HEADER_INFO, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_GENDER_INFO_TEXT, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_GENDER_IDENTITY_TEXT, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_BIRTH_PLACE_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_DEFERALS_INFO_HIGHLIGHTED, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_ETHNICITY_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_ETHNICITY_SUB_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_PHONE_INFO_TEXT, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_EXACT_LOCATION_INFO_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_CHILDREN_SUB_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_FAMILY_MEMBERS_INFO, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_PARENT_GRAD_DEGREE_LABEL, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_PARENT_DEGREE_LABEL, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_ALL_UPLOAD_FILE_TYPE, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_MAX_PARENTS, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_MAX_CHILDREN, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_MILITARY_SUB_HEADER, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_MAX_GSB_CONNECTIONS, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_ALL_UPLOAD_MAX_FILE_SIZE, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_ALL_UPLOAD_MAX_FILE_SIZETYPE, new List<String>{programCode}),
                                                                   String.format(ADMConstants.TM_APP_FAMILY_SECTION_OTHER_GSBRELATIONSHIP_DEGREE_YEAR_PICKLIST_VALUE, new List<String>{programCode})
                                                                   })); 
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
                String.format(ADMConstants.TM_APP_ACTIVITIES, new List<String>{programCode})));   
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
                    String.format(ADMConstants.TM_APP_ESSAY, new List<String>{programCode})));
        // additional information
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_ADDITIONAL_INFORMATION, new List<String>{programCode})));
        // Get recommendation settings        
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_RECOMMENDATIONS, new List<String>{programCode})));  
        // Get Board Exam settings        
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_BOARD_EXAM, new List<String>{programCode})));  
        // Get Financial Information settings        
        this.TM_TEXT_OBJ.putAll(TextManagerUtils.getAppTextAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP_FINANCIAL_INFO, new List<String>{programCode}))); 
        // Get application settings
        this.TM_SETTING_OBJ = TextManagerUtils.getAppSettingAllKeys(CoreConstants.ApplicationID.adm.name().toLowerCase(), 
            String.format(ADMConstants.TM_APP, new List<String>{programCode}));


    }

    /**
     * Initialize printable view information
     *
     * @return  None
     */
    private void initializePrintableView() {

        ADMApplicationService appService = new ADMApplicationService(this.programCode);
        List<Id> applicationIds = new List<Id>{this.admApplication.Id};
        
        Set<String> setCountriesNeedStates = new Set<String>();
        setCountriesNeedStates.addAll(this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_COUNTRIES_NEED_STATES, 
            new List<String>{this.programCode})).toLowerCase().split(CoreConstants.DELIMITER_COMMA));

        //get background - personal information
        ADMApplicantService applicantService = new ADMApplicantService(this.programCode);
        List<ADMApplicantService.PersonalInformationVO> applicantPersonalInfos;
        applicantPersonalInfos = applicantService.getPersonalInformationByApplicantIds(new List<Id>{this.admApplicant.Id}, this.admApplicant.gender__c, setCountriesNeedStates, this.admApplicant.birth_country_id__c);
        applicantPersonalInfos[0].setgender(this.admApplicant.gender__c);           
        this.applicantPersonalInformation = applicantPersonalInfos[0];

        //List of application personal Information VOs
        ADMApplicationService applicationService = new ADMApplicationService(this.programCode);
        List<ADMApplicationService.PersonalInformationVO> personalInfos;
        personalInfos = applicationService.getPersonalInformationByApplicationIds(new List<Id>{this.admApplication.Id}, this.admApplication.primary_citizenship__c, this.admApplication.current_country__c, this.admApplication.current_city__c, setCountriesNeedStates);
        personalInfos[0].setPrimaryCitizenship(this.admApplication.primary_citizenship__c);
        personalInfos[0].setCurrentCountry(this.admApplication.current_country__c);
        personalInfos[0].setCurrentCity(this.admApplication.current_city__c);
        this.personalInformation = personalInfos[0];

        //get background - family information
        ADMFamilyService familyService = new ADMFamilyService(this.programCode);
        this.admCurrentParents = familyService.getParentsByApplicationIds(applicationIds, 0, this.expandId);
        this.admCurrentChildren = familyService.getChildrenByApplicationIds(applicationIds, 0, this.expandId);
        this.admCurrentGSBConnections = familyService.getGSBConnectionByApplicationIds(applicationIds, 0, this.expandId);
        this.admFamily = familyService.initializeParents(admCurrentParents, 0);
        this.admChildren = familyService.initializeChildren(admCurrentChildren, 0);
        this.admGSBConnection = familyService.initializeGSBConnections(admCurrentGSBConnections, 0);

        // High level Spouse information
        List<ADMFamilyService.SpouseVO> spouseInfos = familyService.getSpouseByApplicationIds(applicationIds); 
        if (spouseInfos != null && !spouseInfos.isEmpty()) {
            this.admSpouse = spouseInfos[0];
        } else {
            this.admSpouse = new ADMFamilyService.SpouseVO();
            this.admSpouse.Spouse.application_id__c = personalInformation.application.Id;                       
        }

        //get application information
        List<ADMApplicationService.ApplicationInformationVO> applicationInfos = 
            appService.getApplicationInformationAndAnyRoundByApplicationIds(new List<Id>{this.admApplication.Id});
        if (applicationInfos != null && !applicationInfos.isEmpty()) this.applicationInformation = applicationInfos[0];
        else this.applicationInformation = new ADMApplicationService.ApplicationInformationVO();
       
        //ADM-4793  pdf app submitted date was not reflected
        if (!String.isBlank(System.currentPagereference().getParameters().get(ADMConstants.PDF_GENERATION_URL_PARAM_NAME.appSubmittedDate.name()))) 
            this.applicationInformation.application.application_submitted_date__c=DateTime.valueOf(System.currentPagereference().getParameters().get(ADMConstants.PDF_GENERATION_URL_PARAM_NAME.appSubmittedDate.name()));
        //get Financial information
        List<ADMApplicationService.FinInfoResultsVO> financialInfos = 
            appService.getFinInfoResultsInformationByApplicationIds(new List<Id>{this.admApplication.Id});
        if (financialInfos != null && !financialInfos.isEmpty()) this.financialInformation = financialInfos[0];
        else this.financialInformation = new ADMApplicationService.FinInfoResultsVO();

        //get Financial information
        List<ADMApplicationService.BoardExamsVO> boardExamInfos = 
            appService.getBoardExamsInformationByApplicationIds(new List<Id>{this.admApplication.Id});
        if (boardExamInfos != null && !boardExamInfos.isEmpty()) this.boardexamInformation = boardExamInfos[0];
        else this.boardexamInformation = new ADMApplicationService.BoardExamsVO();

        //get education related information     
        ADMEducationService eduService = new ADMEducationService(this.programCode);
        List<ADMApplicationService.EducationInformationVO> educationInfos = appService.getEducationInformationByApplicationIds(new List<Id>{this.admApplication.Id}, setCountriesNeedStates);
        if (educationInfos != null && !educationInfos.isEmpty()) {
            this.educationInformation = educationInfos[0];
        } else {
            this.educationInformation = new ADMApplicationService.EducationInformationVO();
        }           
                    
        this.admCurrentColleges = eduService.getCollegesByApplicationIds(applicationIds, maximumColleges, this.expandId, 
                                    this.TM_SETTING_OBJ.get(String.format(ADMConstants.TM_APP_COUNTRIES_NEED_STATES, 
                                                new List<String>{programCode})));
        this.admCollege = eduService.initializeColleges(admCurrentColleges, maximumColleges); 
        this.admCurrentLanguages = eduService.getLanguagesByApplicationIds(applicationIds, maximumLanguages, this.expandId);
        this.admLanguage = eduService.initializeLanguages(admCurrentLanguages, maximumLanguages);

        //get test scores
        List<ADMTestResultsService.TestResultsVO> testResultVOList = appService.getTestResultsInformationByApplicationIds(applicationIds);
        if (testResultVOList != null && !testResultVOList.isEmpty()) this.admTestResults = testResultVOList[0];
        else this.admTestResults = new ADMTestResultsService.TestResultsVO(); 

        //get employment information
        ADMEmploymentService empService = new ADMEmploymentService(this.programCode);
        List<ADMApplicationService.EmploymentInformationVO> employmentInfos = appService.getEmploymentInformationByApplicationIds(
            new List<Id>{this.admApplication.Id});
        this.employmentInformation = employmentInfos[0];
        
        this.admEmployment = empService.initializeEmployment(0);
        this.admCurrentEmployments = empService.getEmploymentByApplicationIds(applicationIds, setCountriesNeedStates, 0, 
            null);

        //get activities and interests
        ADMActivityService actService = new ADMActivityService(programCode);
        List<ADMApplicationService.ActivityInformationVO> activityInfos = appService.getActivityInformationByApplicationIds(new List<Id>{this.admApplication.Id});
        if (activityInfos != null && !activityInfos.isEmpty()) this.activityInformation = activityInfos[0];
        else this.activityInformation = new ADMApplicationService.ActivityInformationVO();   
        admActivity= new ADMActivityService.ActivityVO(0);
        this.admCurrentActivities = actService.getActivitiesByApplicationIds(applicationIds, 0, this.expandId);


        //get award and honors information
        ADMAwardsAndHonorsService awardsAndHonorsService = new ADMAwardsAndHonorsService(this.programCode);
        this.admCurrentAwardsAndHonors = awardsAndHonorsService.getAwardsAndHonorsByApplicationIds(applicationIds, 0,
             null);
        this.admAwardsAndHonors = awardsAndHonorsService.initializeAwardsAndHonors(admCurrentAwardsAndHonors, 0);

        //get essays
        List<ADMApplicationService.EssayVO> essayVOList = appService.getEssayInformationByApplicationIds(
            new List<Id>{this.admApplication.Id});
        if (essayVOList != null && !essayVOList.isEmpty()) this.essayVO = essayVOList[0];
        else this.essayVO = new ADMApplicationService.EssayVO();    
        //get additional information
        List<ADMApplicationService.AdditionalInformationVO> additionalInfos = appService.getAdditionalInformationByApplicationIds(
            new List<Id>{this.admApplication.Id});
        if (additionalInfos != null && !additionalInfos.isEmpty()) this.additionalInformation = additionalInfos[0];
        else this.additionalInformation = new ADMApplicationService.AdditionalInformationVO();
        //get letters of reference
        ADMRecommendationsService recommendationsService = new ADMRecommendationsService(this.programCode);
        this.admCurrentRecommendations = recommendationsService.getRecommendationsByApplicationIds(applicationIds, this.navigationSectionId, 0, this.expandId);
        
        // Fetch field settings for Essays, Application Information and Background pages. We know the base page controller is already doing but it is loading
        // settings of "ADMApplicationPrintableView" page. In order to show question and answer we need to know the section is visible for that page or not.
        // Hence fetching field settings and loading into the main map.
        GSBApplicationPageConfiguration configurations = new GSBApplicationPageConfiguration(this.programCode, 
                    new List<String>{
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMEssays.name(), ADMConstants.ADM_HOST_PAGE_NAMES.ADMApplicationInformation.name(), 
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMPersonalFamilyInformation.name(),
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMEmploymentInformation.name(),
                            ADMConstants.ADM_HOST_PAGE_NAMES.ADMEducationInformation.name()
                    });
        //Map<String, GSBApplicationPageConfiguration.FieldAndSectionSettingsVO> applicationViewPageSettings = new Map<String, GSBApplicationPageConfiguration.FieldAndSectionSettingsVO>();
        //applicationViewPageSettings.putAll(configurations.objectFieldSettingsMap);
        //applicationViewPageSettings.add(this.pageFieldSettings.objectFieldSettingsMap);
        this.FIELD_SECTION_SETTINGS_MAP.putAll(configurations.objectFieldSettingsMap);
    }
}